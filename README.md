aurs
----
aurs is an AUR helper with the following features:

  - It doesn't wrap `pacman`. Use `aurs` for the AUR and `pacman` for
    the rest. This allows `aurs` to provide an intuitive command line
    interface to the AUR.
    
  - It doesn't ask you to verify the same PKGBUILD twice. `aurs`
    remembers which PKGBUILDs you trust and writes this information to
    a single file that can even be shared across different machines.
    
  - It minimizes user interaction and unnecessary output. `aurs` tries
    to do it's thing without bothering you.

This is still very much work in progress, so more is definitely to come.

### Usage Examples

Please see `aurs --help` for a complete reference of the command line
interface.

Search:

    aurs search emacs git
    
Info:

    aurs info emacs-git
    
Show currently installed packages:

    aurs list
    
Install a package:

    aurs install emacs-git
    
Upgrade all AUR packages:

    aurs update

### Installation

To build `aurs` from source, first install `rust` and `clang` and run:

    cargo build --release
    cp target/release/aurs /whereever/you/want/the/binary
