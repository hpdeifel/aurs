# 0.5.0 (2019-11-01)

Features:

  - Add new `fetch` command to download AUR repositories without installing
    them.

Improvements:

  - Switch code base to Rust's 2018 edition.
  - Use `prettytable` crate instead of reimplementing tables formatting.
  - Bump `serde` dependency to 1.0.102 to fix a compile error.

# 0.4.2 (2018-09-18)

Improvements:

  - Bump reqwest dependency to version 0.9.0. This fixes build and
    runtime compatability with openssl 1.1.1.

# 0.4.1 (2018-08-16)

Bugfixes:

  - Handle empty description in search results and package infos
    correctly.

# 0.4.0 (2018-05-03)

Features:

  - Allow re-installation with `install` command: Packages can now be
    installed even if they are already present in the system. Use the
    new flag `--needed` to opt out.

Improvements:

  - Remove `get` command: This was a pretty low level command with an
    ill-designed interface.
  - Beautify "Fetching xyz..." messages.

# 0.3.0 (2018-04-26)

Features:

  - Add `cache` subcommand to inspect and clear aurs' cache.
  - Allow updating individual packages with `update`.
  - Add `--ignore` option to `update`, to ignore certain packages
    during a system update.
  - Add `-s` option to `search`, to sort by different criteria.
  - Add `-r` option to `search`, to reverse search order.

Improvements:

  - Automatically trust empty diffs.

# 0.2.1

  - Fix version in --version output

# 0.2.0

  - Show diff between old and new version on updates
  - Save and re-apply edits to PKGBUILDs
  - Cache SRCINFO files to make updates a little faster

# 0.1.0

  - Initial release
