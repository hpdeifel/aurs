use std::collections::{HashMap, HashSet, VecDeque};
use std::cmp::Ordering;

use petgraph::graph::{DiGraph, NodeIndex};
use petgraph::{algo, Direction};
use failure::{self, format_err, bail};
use colored::{Colorize, Color};

use crate::srcinfo::{Dependency, Package, Version, VersionRequirement};
use crate::alpm;
use crate::aur_api;
use crate::cache::Cache;
use crate::pacman;
use crate::utils::*;
use prettytable::{Table,self, cell};
use crate::output::spinner;

struct PkgInfo {
    base: String,
    version: Version,
    /// For updates, this should be set tho the currently installed version.
    old_version: Option<Version>,
}

#[derive(PartialEq)]
enum PkgKind {
    Installed,
    Pacman,
    Aur(AurPackage),
}

#[derive(PartialEq)]
struct AurPackage {
    pkg: Package,
    /// `true` if the version information in this package is already updated
    /// from VCS.
    vcs_updated: bool,
}

impl PkgKind {
    /// Will panic if not Aur
    fn unwrap(&self) -> &Package {
        match *self {
            PkgKind::Installed => panic!("Installed package has no AUR infos"),
            PkgKind::Pacman => panic!("Repo-package has no AUR infos"),
            PkgKind::Aur(ref pkg) => &pkg.pkg,
        }
    }
}

/// What to do if a package is already installed.
#[derive(PartialEq)]
enum ReinstallPolicy {
    /// Reinstall unconditionally.
    Always,
    /// Reinstall only if the new version is newer than the old one. This
    /// corresponds to `--needed` in pacman.
    Needed,
}

/// Used by solver and installer to make decisions on individual packages.
///
/// Should be implemented differently for update and install for example.
trait SolverPolicy {
    /// Decide whether a package should be installed explicitly.
    ///
    /// Given a package name as argument, returns `true` if this package should
    /// be installed with `pacman --asexplicit` or false for `pacman --asdeps`.
    fn should_install_as_explicit(&self, _: &str) -> bool;

    /// Decide whether the source of a VCS package should be refreshed.
    ///
    /// Given the name of a VCS package, returns `true` if the package source
    /// should be updated with `git pull`, etc. Returning `false` will use the
    /// commit that was last checked out on disk.
    fn should_update_vcs(&self, _: &str) -> bool;

    /// Decide whether the package should be reinstalled.
    ///
    /// See pacmans `--needed` flag.
    fn should_reinstall(&self, _: &str) -> ReinstallPolicy;
}


//////////////////////////////////////////////////////////////////////
//
// Updates
//
//////////////////////////////////////////////////////////////////////

struct UpdatePolicy<'a> {
    /// We need a reference to the local database to decide if packages are
    /// currently marked as explicitly installed.
    local_db: &'a alpm::Database<'a>,

    /// User specified the --devel flag
    devel: bool,

    /// Targets that are given on the command line
    targets: HashSet<String>,
}

impl<'a> SolverPolicy for UpdatePolicy<'a> {
    /// Only install a package as explcit if it was previously installed as
    /// explcit.
    fn should_install_as_explicit(&self, pkgname: &str) -> bool {
        is_installed_explicit(self.local_db, pkgname)
    }

    /// If --devel was provided, update all VCS repositories. Otherwise update
    /// only explicit targets.
    fn should_update_vcs(&self, pkgname: &str) -> bool {
        self.devel || self.targets.contains(pkgname)
    }

    /// Only ever reinstall over installed packages if the new version is newer
    /// than the installed one.
    fn should_reinstall(&self, _pkgname: &str) -> ReinstallPolicy {
        ReinstallPolicy::Needed
    }
}

pub fn update_all(local_db: alpm::Database<'_>, cache: &mut Cache, devel: bool, ignores: &[&str]) -> Result<()> {
    let aur_infos = aur_api::info(&pacman::foreign_packages()?)?;
    let pkg_names: Vec<_> = aur_infos.iter().map(|x| x.name.as_str()).collect();

    // TODO: Install retrieves the info for all packages again, although we
    // already have them. => Figure out a way to pass aur_api::Info structs to
    // install().

    let policy = UpdatePolicy {
        local_db: &local_db,
        devel: devel,
        targets: HashSet::new(),
    };

    install_impl(&local_db, cache, &pkg_names, ignores, &policy)
}

/// Update specific packages and their dependencies.
///
/// Errors if a packages in `packages` is not installed.
///
/// Packages are only marked as explicit if they were previously marked as
/// explicit. VCS packages are updated if they are either in the `packages` list
/// or if `devel` is true.
pub fn update(
    local_db: alpm::Database<'_>,
    cache: &mut Cache,
    packages: &[&str],
    devel: bool,
    ignores: &[&str]
) -> Result<()> {
    let targets: HashSet<_> = packages.iter().map(|&x| x.to_owned()).collect();

    for pkg in packages {
        if local_db.get_pkg(pkg).is_err() {
            bail!("{} is not an installed package", pkg);
        }
    }

    let policy = UpdatePolicy {
        local_db: &local_db,
        devel: devel,
        targets: targets, // TODO
    };

    install_impl(&local_db, cache, packages, ignores, &policy)
}


//////////////////////////////////////////////////////////////////////
//
// Install
//
//////////////////////////////////////////////////////////////////////

struct InstallPolicy<'a> {
    /// We need the local database to decide if a package was previously installed as explicit.
    local_db: &'a alpm::Database<'a>,

    /// Targets specified on the command line
    targets: HashSet<String>,

    /// Whether the `--devel` flag was present
    devel: bool,

    /// Whether the `--asdeps` flag was present
    asdeps: bool,

    /// Whether the `--needed` flag was present
    needed: bool,
}

impl<'a> SolverPolicy for InstallPolicy<'a> {
    /// Install package as explicit if it's a target and `asdeps` was not
    /// provided or if it was already installed as explicit and is not a
    /// target.
    fn should_install_as_explicit(&self, pkgname: &str) -> bool {
        let is_target = self.targets.contains(pkgname);

        (!self.asdeps && is_target)   
            || (!is_target && is_installed_explicit(self.local_db, pkgname))
    }

    /// If --devel was provided, update all VCS repos, otherwise only targets.
    fn should_update_vcs(&self, pkgname: &str) -> bool {
        self.devel || self.targets.contains(pkgname)
    }

    /// Always reinstall targets, otherwise only when needed.
    fn should_reinstall(&self, pkgname: &str) -> ReinstallPolicy {
        if !self.needed && self.targets.contains(pkgname) {
            ReinstallPolicy::Always
        } else {
            ReinstallPolicy::Needed
        }
    }
}

pub fn install(
    local_db: alpm::Database<'_>,
    cache: &mut Cache,
    packages: &[&str],
    asdeps: bool,
    devel: bool,
    needed: bool,
) -> Result<()> {
    let targets: HashSet<_> = packages.iter().map(|&x| x.to_owned()).collect();
    // test that all packages are actually in the AUR
    let aur_packages: HashSet<String> = aur_api::info(packages)?.into_iter()
        .map(|i| i.name)
        .collect();

    for pkg in packages {
        if !aur_packages.contains(*pkg) {
            bail!("{} is not an AUR package", pkg);
        }
    }

    let policy = InstallPolicy {
        local_db: &local_db,
        targets,
        devel,
        asdeps,
        needed,
    };

    install_impl(&local_db, cache, packages, &[], &policy)
}

fn install_impl<P: SolverPolicy>(
    local_db: &alpm::Database<'_>,
    cache: &mut Cache,
    packages: &[&str],
    ignores: &[&str],
    policy: &P,
) -> Result<()>
{
    let state = solve(local_db, cache, packages, ignores, policy)?;

    let to_install: Vec<_> = state
        .pkgkind
        .iter()
        .filter_map(|(x, y)| if *y == PkgKind::Pacman { Some(x) } else { None })
        .collect();

    // Remove all non-aur packages from graph
    let mut aur_graph = state.depgraph.filter_map(
        |_, pkg| {
            state.pkgkind.get(pkg).and_then(|kind| match *kind {
                PkgKind::Pacman | PkgKind::Installed => None,
                _ => Some(pkg.clone()),
            })
        },
        |_, _| Some(()),
    );

    aur_graph.reverse();

    let from_aur_ids = algo::toposort(&aur_graph, None).map_err(|_| {
        failure::err_msg("Cycle in AUR dependencies detected") // TODO Better error with cycle
    })?;

    let from_aur: Vec<_> = from_aur_ids
        .into_iter()
        .map(|id| aur_graph.node_weight(id).unwrap())
        .collect();

    if from_aur.is_empty() {
        println!("Nothing to do, all up to date");
        return Ok(());
    }

    if !to_install.is_empty() {
        println!("Will install the following repo packages:");
        for &pkg in &to_install {
            let node = state.seen_packages[pkg];
            let dependencies: Vec<_> = state.depgraph
                .neighbors_directed(node, Direction::Incoming)
                .map(|n| state.depgraph.node_weight(n).unwrap())
                .cloned()
                .collect();

            print!("  {}", pkg.bold());
            if !dependencies.is_empty() {
                print!(" (required by {})", dependencies.join(", "))
            }
            println!();
        }
        println!();
    }

    println!("Will install the following AUR packages:");
    let mut table = Table::new();
    for &pkg in &from_aur {
        let info = &state.pkgcache[pkg];
        let mut row = prettytable::Row::empty();
        row.add_cell(cell!(b->format!(" {} ", pkg)));

        if let Some(ref old_ver) = info.old_version {
            let (left, right) = render_diff(
                &Version::diff(old_ver, &info.version),
                Color::Red,
                None,
                Color::Green,
                None,
            );
            row.add_cell(cell!(format!("{}", left)));
            row.add_cell(cell!("->"));
            row.add_cell(cell!(format!("{}", right)));

            if old_ver.compare(&info.version) != Ordering::Less {
                row.add_cell(cell!(bFgr->"(reinstalling)"));
            }
        } else {
            row.add_cell(cell!(Fgg->info.version.to_string()));
        }
        table.add_row(row);
    }
    let format = prettytable::format::FormatBuilder::new()
        .column_separator(' ')
        .left_border(' ')
        .build();
    table.set_format(format);
    table.print_tty(false);

    //println!("{}", table);

    println!();
    let answer = ask(format_args!("Go ahead? (Y/n): "))?.to_lowercase();
    if !(answer.is_empty() || answer == "y" || answer == "yes") {
        println!("Ok");
        return Ok(());
    }

    if !to_install.is_empty() {
        pacman::install_from_repo(&to_install)?;
    }

    for pkg in from_aur {
        let info = &state.pkgcache[pkg];
        let pkgfile = match cache.prebuild_package(pkg, &info.version) {
            Some(x) => {
                println!("Using existing build of {}", pkg.bold());
                x
            }
            None => {
                println!("Building {}", pkg.bold());
                cache.build(&info.base)?;
                cache.prebuild_package(pkg, &info.version).ok_or_else(|| {
                    failure::err_msg("Build did not produce the required package archive")
                })?
            }
        };

        println!("Installing {}", pkg.bold());
        pacman::install_pkg_file(&pkgfile, !policy.should_install_as_explicit(pkg))?;
    }

    Ok(())
}

fn solve<'a, P: SolverPolicy>(
    local_db: &'a alpm::Database<'a>,
    cache: &mut Cache,
    packages: &[&str],
    ignores: &[&str],
    policy: &'a P,
) -> Result<State<'a, P>>
{
    let mut state = State::new(local_db, policy);

    for pkg in packages {
        state.add_package(pkg);
    }

    let ignore_set: HashSet<&str> = ignores.into_iter().cloned().collect();

    while let Some(pkg) = state.queue.pop_front() {
        if ignore_set.contains(pkg.as_str()) {
            continue;
        }

        // package is part of a split package that we have seen earlier
        if state.pkgcache.contains_key(pkg.as_str()) {
            state.mark_installed_or_add_dependencies(cache, pkg.as_str())?;
            continue;
        }

        // package is in the AUR
        if let Some(pkginfo) = aur_info1(pkg.as_str())? {
            spinner::set_message(&format!("Fetching {}...", pkg.green()));

            cache.fetch_and_trust(&pkginfo.package_base)?;
            let devel = policy.should_update_vcs(&pkg);

            add_srcinfo(&mut state.pkgcache, &mut state.pkgkind, cache, &pkginfo.package_base, devel)?;

            state.mark_installed_or_add_dependencies(cache, &pkg)?;

            continue;
        }

        // package is already installed
        if state.is_installed(&pkg) {
            state.mark_as_installed(&pkg)?;
            continue;
        }

        // package is available in repo
        if let Some(version) = pacman::repo_version(&pkg)? {
            state.mark_as_repo(&pkg, version);
            continue;
        }

        // unknown package
        bail!("Package {} not found in AUR or pacman repo", pkg);
    }

    Ok(state)
}

type Index = NodeIndex<u32>;

struct State<'a, P: SolverPolicy> {
    // Infos about all packages we know about: This can include parts of a
    // split package that we are not actually installing.
    pkgcache: HashMap<String, PkgInfo>,
    pkgkind: HashMap<String, PkgKind>,
    // The packages that we have processed so far as part of the dependency
    // graph
    seen_packages: HashMap<String, Index>,

    depgraph: DiGraph<String, Option<VersionRequirement>>,
    queue: VecDeque<String>,
    local_db: &'a alpm::Database<'a>,
    policy: &'a P,
}

impl<'a, P: SolverPolicy> State<'a, P> {
    fn new(local_db: &'a alpm::Database<'a>, policy: &'a P) -> Self {
        State {
            pkgcache: HashMap::new(),
            pkgkind: HashMap::new(),
            seen_packages: HashMap::new(),
            depgraph: DiGraph::new(),
            queue: VecDeque::new(),
            local_db,
            policy,
        }
    }

    fn mark_installed_or_add_dependencies(&mut self, cache: &mut Cache, pkg: &str) -> Result<()> {
        if let Ok(localpkg) = self.local_db.get_pkg(pkg) {
            let local_version = Version::parse(&localpkg.version());
            let reinstall =
                self.policy.should_reinstall(pkg) == ReinstallPolicy::Always
                || self.pkgcache[pkg].version.compare(&local_version) == Ordering::Greater;

            if reinstall {
                self.pkgcache.get_mut(pkg).unwrap().old_version = Some(local_version);
            } else {
                self.pkgkind.insert(pkg.to_string(), PkgKind::Installed);
                self.pkgcache.get_mut(pkg).unwrap().version = local_version;
                return Ok(());
            }
        }
        // This package is already newer than the installed version or wasn't
        // installed to begin with. => Update vcs to get the newest version.
        // (This would happen anyway during build and this way we get accurate
        // version information during dependency resolution)
        let should_update_vcs = {
            if let PkgKind::Aur(ref aurpkg) = self.pkgkind[pkg] {
                aurpkg.pkg.is_vcs() && !aurpkg.vcs_updated
            } else {
                false
            }
        };
        let pkgbase = self.pkgcache[pkg].base.clone();

        if should_update_vcs {
            let old_version = self.pkgcache[pkg].old_version.clone();
            add_srcinfo(&mut self.pkgcache, &mut self.pkgkind, cache, &pkgbase, true)?;
            self.pkgcache.get_mut(pkg).unwrap().old_version = old_version;
        }
        // Add dependencies to graph and queue
        let node = self.seen_packages[pkg];
        let depends: Vec<Dependency> = {
            let aurpkg = self.pkgkind
                .get(pkg)
                .ok_or_else(|| format_err!("SRCINFO doesn't contain expected package {}", pkg))?
                .unwrap();
            aurpkg
                .depends
                .iter()
                .chain(aurpkg.makedepends.iter())
                .cloned()
                .collect()
        };

        for dep in depends {
            let depnode = self.add_package(&dep.name);
            self.depgraph.add_edge(node, depnode, dep.version);
        }
        Ok(())
    }

    // Add to seenmap, queue and graph
    fn add_package(&mut self, pkg: &str) -> Index {
        if let Some(idx) = self.seen_packages.get(pkg) {
            return *idx;
        }

        let node = self.depgraph.add_node(pkg.to_string());
        self.queue.push_back(pkg.to_string());
        self.seen_packages.insert(pkg.to_string(), node);

        node
    }

    /// Returns `true` if `pkg` is either installed or provided by an installed
    /// package.
    fn is_installed(&self, pkg: &str) -> bool {
        self.local_db.get_pkg(pkg).is_ok() || self.local_db.find_satisfier(pkg).is_some()
    }

    fn mark_as_installed(&mut self, pkg: &str) -> Result<()> {
        if let Ok(localpkg) = self.local_db.get_pkg(pkg) {
            self.pkgkind.insert(pkg.to_string(), PkgKind::Installed);
            self.pkgcache.insert(
                pkg.to_string(),
                PkgInfo {
                    base: pkg.to_string(),
                    version: Version::parse(&localpkg.version()),
                    old_version: None,
                },
            );
        }
        Ok(())
    }

    fn mark_as_repo(&mut self, pkg: &str, version: Version) {
        self.pkgkind.insert(pkg.to_string(), PkgKind::Pacman);
        self.pkgcache.insert(
            pkg.to_string(),
            PkgInfo {
                base: pkg.to_string(),
                version,
                old_version: None,
            },
        );
    }
}

fn aur_info1(name: &str) -> Result<Option<aur_api::Info>> {
    let res = aur_api::info(&[name])?;
    Ok(res.into_iter().next())
}

fn add_srcinfo(
    pkgcache: &mut HashMap<String, PkgInfo>,
    pkgkind: &mut HashMap<String, PkgKind>,
    cache: &mut Cache,
    pkgbase: &str,
    devel: bool
) -> Result<()> {
    let srcinfo_packages = cache.get_srcinfo(pkgbase, devel)?;

    // Add all parts of the split package to pkgcache, pkgkind
    for (name, pkg) in srcinfo_packages {
        pkgcache.insert(
            name.clone(),
            PkgInfo {
                version: pkg.version.clone(),
                base: pkgbase.to_string(),
                old_version: None,
            },
        );

        let aur_pkg = AurPackage { pkg, vcs_updated: devel };
        pkgkind.insert(name.clone(), PkgKind::Aur(aur_pkg));
    }

    Ok(())
}

fn is_installed_explicit(local_db: &alpm::Database<'_>, pkg: &str) -> bool {
    match local_db.get_pkg(pkg) {
        Ok(res) => res.reason() == alpm::PackageReason::Explicit,
        Err(_) => false
    }
}
