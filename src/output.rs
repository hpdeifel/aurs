/// A self-overwriting terminal message
pub mod spinner {
    use termion::{clear, cursor};

    #[derive(PartialEq)]
    enum SpinnerState {
        Running,
        Inhibited,
        Disabled,
    }

    impl SpinnerState {
        fn disable(&mut self) {
            *self = SpinnerState::Disabled;
        }

        fn inhibit(&mut self) {
            if *self != SpinnerState::Disabled {
                *self = SpinnerState::Inhibited;
            }
        }

        fn set_running(&mut self) {
            if *self != SpinnerState::Disabled {
                *self = SpinnerState::Running;
            }
        }

        fn is_running(&self) -> bool {
            *self == SpinnerState::Running
        }
    }

    static mut SPINNER_STATE: SpinnerState = SpinnerState::Inhibited;

    /// Print `msg` on `stdout` and overwrite the previous message if there was
    /// one.
    pub fn set_message(msg: &str) {
        if unsafe { SPINNER_STATE.is_running() }  {
            print!("{}{}", cursor::Up(1), clear::AfterCursor);
        }

        unsafe {
            SPINNER_STATE.set_running();
        }
        println!("{}", msg);
    }

    /// Inhibit the spinner.
    ///
    /// Use this if you want to use stdout while there is a spinner active, so
    /// that the next `set_message` doesn't overwrite what you print.
    pub fn inhibit() {
        unsafe {
            SPINNER_STATE.inhibit();
        }
    }

    /// Disable the spinner altogether.
    ///
    /// Note that it cannot be enabled anymore. In this state every message sent
    /// to the spinner is normally printed to `stdout`. Use this e.g. if
    /// `stdout` is not a terminal.
    pub fn disable() {
        unsafe {
            SPINNER_STATE.disable();
        }
    }
}
