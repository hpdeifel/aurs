pub mod alpm;
pub mod aur_api;
pub mod pacman;
pub mod cache;
pub mod trust;
pub mod solver;
pub mod srcinfo;
pub mod utils;
pub mod output;

use crate::srcinfo::Version;
use crate::trust::TrustDB;
use crate::utils::Result;

use std::env;
use std::process;
use std::collections::HashMap;
use std::cmp;

use clap::{App, Arg, SubCommand, crate_version};
use colored::Colorize;
use colored;
use chrono;
use failure::bail;
use humansize::{FileSize, file_size_opts};
use prettytable::{Table, row, cell};
use xdg;
use termion;


fn main() {
    // Don't output colors if stdout is now a terminal (e.g when piped to
    // another program).
    if !termion::is_tty(&std::io::stdout()) {
        colored::control::set_override(false);
        output::spinner::disable();
    }

    let search_command = SubCommand::with_name("search").about("Search the AUR")
        .arg(
            Arg::with_name("sort-by")
                .help("Sort results by ATTRIBUTE")
                .long("sort-by")
                .short("s")
                .default_value("name")
                .value_name("ATTRIBUTE")
                .possible_values(&["name", "n", "votes", "v", "popularity", "p"])
        )
        .arg(
            Arg::with_name("reverse")
                .help("Show results in reverse order")
                .long("reverse")
                .short("r")
        )
        .arg(
            Arg::with_name("KEYWORD")
                .help("Keyword to search for")
                .multiple(true)
                .required(true),
        );

    let info_command = SubCommand::with_name("info")
        .about("Print detailed package information")
        .arg(Arg::with_name("PACKAGE").required(true).multiple(true));

    let list_command = SubCommand::with_name("list")
        .about("List packages installed from the AUR")
        .arg(
            Arg::with_name("verbose")
                .long("verbose")
                .short("v")
                .help("Show more information per package (e.g the description)"),
        );

    let devel_arg = Arg::with_name("devel")
        .long("devel")
        .help("Update VCS packages to their latest commit");

    let install_command = SubCommand::with_name("install")
        .about("install AUR packages")
        .arg(Arg::with_name("PACKAGE").required(true).multiple(true))
        .arg(
            Arg::with_name("asdeps")
                .long("asdeps")
                .help("Install targets as if they were dependencies"),
        )
        .arg(
            Arg::with_name("needed")
                .long("needed")
                .help("Do not reinstall targets that are already up-to-date"),
        )
        .arg(
            Arg::with_name("rebuild")
                .long("rebuild")
                .help("Always rebuild packages, even if they're already cached"),
        )
        .arg(&devel_arg);

    let update_command = SubCommand::with_name("update")
        .about("update all AUR packages")
        .arg(Arg::with_name("PACKAGE").multiple(true))
        .arg(
            Arg::with_name("ignore")
                .long("ignore")
                .help("Ignore updates for a package")
                .value_name("PACKAGE")
                .takes_value(true)
                .multiple(true)
        )
        .arg(&devel_arg);

    let build_command = SubCommand::with_name("build")
        .about("build already downloaded package")
        .arg(
            Arg::with_name("needed")
                .long("needed")
                .help("Don't rebuild already cached packages"),
        )
        .arg(Arg::with_name("PACKAGE").required(true).multiple(true));

    let fetch_command = SubCommand::with_name("fetch")
        .about("Download package repository into cache")
        .arg(Arg::with_name("PACKAGE").required(true));

    let cache_command = make_cache_command();

    let mut app = App::new("aurs")
        .version(crate_version!())
        .about("Another AUR helper")
        .global_setting(clap::AppSettings::ColoredHelp)
        .global_setting(clap::AppSettings::VersionlessSubcommands)
        .subcommand(search_command)
        .subcommand(info_command)
        .subcommand(list_command)
        .subcommand(install_command)
        .subcommand(build_command)
        .subcommand(update_command)
        .subcommand(fetch_command)
        .subcommand(cache_command);

    let matches = app.get_matches_from_safe_borrow(env::args_os())
        .unwrap_or_else(|e| e.exit());

    let res = match matches.subcommand() {
        ("search", Some(sub_m)) => do_search(sub_m),
        ("info", Some(sub_m)) => do_info(sub_m),
        ("list", Some(sub_m)) => do_list(sub_m),
        ("install", Some(sub_m)) => do_install(sub_m),
        ("build", Some(sub_m)) => do_build(sub_m),
        ("fetch", Some(sub_m)) => do_fetch(sub_m),
        ("update", Some(sub_m)) => do_update(sub_m),
        ("cache", Some(sub_m)) => do_cache(sub_m),
        _ => {
            let _ = app.print_help();
            println!();
            process::exit(0);
        }
    };

    res.unwrap_or_else(|e| {
        eprint!("Error: ");
        for cause in e.iter_chain() {
            eprintln!("{}", cause);
        }
        process::exit(1);
    });
}

type Command = Result<()>;

fn make_cache_command<'a, 'b>() -> App<'a, 'b> {
    let info_command = SubCommand::with_name("info")
        .about("Print current cache size");

    let clear_command = SubCommand::with_name("clear")
        .about("Delete cache to free up disk space");

    SubCommand::with_name("cache")
        .about("Inspect or clear cache")
        .setting(clap::AppSettings::SubcommandRequiredElseHelp)
        .subcommand(info_command)
        .subcommand(clear_command)
}

fn cache() -> Result<cache::Cache> {
    let xdg_dirs = xdg::BaseDirectories::with_prefix("aurs").unwrap_or_else(|e| {
        eprintln!("Error: {}", e);
        process::exit(1);
    });

    let config_dir = xdg_dirs.get_config_home();
    let trust_db = TrustDB::load(&config_dir.join("trust/"), &config_dir.join("trust.toml"))?;

    let cachedir = xdg_dirs.get_cache_home();
    cache::Cache::make(&cachedir, trust_db)
}

fn matches_all(info: &aur_api::SearchResult, keywords: &[&str]) -> bool {
    for key in keywords {
        if !(info.name.contains(key) || info.description.as_ref().map(|s| s.contains(key)) == Some(true)) {
            return false;
        }
    }

    true
}

fn do_search(matches: &clap::ArgMatches<'_>) -> Command {
    let alpm_handle = alpm::Alpm::new()?;
    let localdb = alpm_handle.get_localdb()?;

    let keywords: Vec<_> = matches.values_of("KEYWORD").unwrap().collect();

    let mut results = aur_api::search(aur_api::SearchField::NameAndDesc, keywords[0])?;

    let sorter: fn(&aur_api::SearchResult, &aur_api::SearchResult) -> cmp::Ordering =
        match matches.value_of("sort-by") {
            Some("name") | Some("n") => |a, b| Ord::cmp(&a.name, &b.name),
            Some("votes") | Some("v") =>
                // Highest votes on top
                |a, b| Ord::cmp(&a.num_votes, &b.num_votes).reverse(),
            Some("popularity") | Some("p") => |a, b| {
                // Highest popularity on top
                PartialOrd::partial_cmp(&a.popularity, &b.popularity)
                    .unwrap_or(cmp::Ordering::Equal)
                    .reverse()
            },
            _ => panic!("Error that shouldn't happen: clap didn't validate `sort-by`")
        };

    results.sort_by(sorter);

    if matches.is_present("reverse") {
        results.reverse();
    }

    for entry in results.iter().filter(|&x| matches_all(x, &keywords[1..])) {
        let mut tags = Vec::new();
        if entry.maintainer == None {
            tags.push("unmaintained".red());
        }
        if entry.out_of_date != None {
            tags.push("out of date".red());
        }
        if localdb.get_pkg(&entry.name).is_ok() {
            tags.push("installed".green());
        }
        if entry.is_split_package() {
            tags.push("split package".purple());
        }
        let date = entry.last_modified.with_timezone(&chrono::offset::Local);
        print!(
            "{} {} {} {} {}",
            entry.name.bold(),
            entry.version.blue().bold(),
            date.format("%Y-%m-%d").to_string().cyan(),
            format!("{}★", entry.num_votes).yellow(),
            format!("{:.2}❤", entry.popularity).yellow()
        );

        if !tags.is_empty() {
            print!("   [");
            let len = tags.len();
            for (i, tag) in tags.into_iter().enumerate() {
                print!("{}{}", tag, if i == len - 1 { "" } else { ", " })
            }
            print!("]");
        }
        println!();

        let desc: &str = match &entry.description {
            None => "(no description)",
            Some(x) => &x
        };
        println!("    {}", desc)
    }

    Ok(())
}

fn do_info(matches: &clap::ArgMatches<'_>) -> Command {
    let pkgs: Vec<_> = matches.values_of("PACKAGE").unwrap().collect();

    let results = aur_api::info(&pkgs)?;
    if results.is_empty() {
        println!("No results");
        return Ok(());
    }

    let len = results.len();
    for (i, info) in results.into_iter().enumerate() {
        print_info(&info);
        if i < len - 1 {
            println!();
        }
    }

    Ok(())
}

fn do_list(matches: &clap::ArgMatches<'_>) -> Command {
    let alpm_handle = alpm::Alpm::new()?;
    let localdb = alpm_handle.get_localdb()?;

    let verbose = matches.is_present("verbose");

    let local_pkgs = pacman::foreign_packages()?;
    let infos = aur_api::info(&local_pkgs)?;

    let mut in_aur = HashMap::new();

    for info in infos {
        in_aur.insert(info.name.clone(), info);
    }

    let mut not_in_aur = Vec::new();

    for name in local_pkgs {
        match in_aur.get(&name) {
            Some(info) => {
                let pkg = localdb.get_pkg(&name)?;
                let version = pkg.version();
                let is_explicit = pkg.reason() == alpm::PackageReason::Explicit;

                let mut tags = Vec::new();

                if info.maintainer == None {
                    tags.push("unmaintained".red());
                }
                if info.out_of_date != None {
                    tags.push("out of date".red());
                }

                print!(
                    "{} {}",
                    if is_explicit {
                        name.bold()
                    } else {
                        name.normal()
                    },
                    version.blue()
                );

                if !tags.is_empty() {
                    print!("   [");
                    let len = tags.len();
                    for (i, tag) in tags.into_iter().enumerate() {
                        print!("{}{}", tag, if i == len - 1 { "" } else { ", " })
                    }
                    print!("]");
                }

                println!();

                if verbose {
                    let desc = info.description.as_ref().map(|x| x.as_ref())
                        .unwrap_or("(no description)");
                    println!("    {}", desc);
                }
            }
            None => {
                not_in_aur.push(name);
            }
        }
    }

    if !not_in_aur.is_empty() {
        if !in_aur.is_empty() {
            println!();
        }
        println!(
            "{} {}",
            "Not from the AUR:".bold().green(),
            not_in_aur.join(", ")
        );
    }

    Ok(())
}

fn do_install(matches: &clap::ArgMatches<'_>) -> Command {
    let mut cache = cache()?;
    if matches.is_present("rebuild") {
        cache.make_pkgcache_volatile();
    }

    let alpm_handle = alpm::Alpm::new()?;
    let localdb = alpm_handle.get_localdb()?;

    let pkgs: Vec<_> = matches.values_of("PACKAGE").unwrap().collect();
    let devel = matches.is_present("devel");
    let asdeps = matches.is_present("asdeps");
    let needed = matches.is_present("needed");

    solver::install(localdb, &mut cache, &pkgs, asdeps, devel, needed)?;

    Ok(())
}

fn do_update(matches: &clap::ArgMatches<'_>) -> Command {
    let mut cache = cache()?;

    let alpm_handle = alpm::Alpm::new()?;
    let localdb = alpm_handle.get_localdb()?;

    let devel = matches.is_present("devel");

    let ignores: Vec<_> = match matches.values_of("ignore") {
        None => Vec::new(),
        Some(values) => values.collect(),
    };

    match matches.values_of("PACKAGE") {
        None => solver::update_all(localdb, &mut cache, devel, &ignores)?,
        Some(values) => {
            let pkgs: Vec<_> = values.collect();
            solver::update(localdb, &mut cache, &pkgs, devel, &ignores)?;
        },
    }


    Ok(())
}

fn do_build(matches: &clap::ArgMatches<'_>) -> Command {
    let cache = cache()?;

    let needed = matches.is_present("needed");

    for pkg in matches.values_of("PACKAGE").unwrap() {
        let aur_infos = aur_api::info(&[pkg])?;
        if aur_infos.is_empty() {
            bail!("Package not found: {}", pkg);
        }
        let aur_info = &aur_infos[0];

        let already_build = cache.prebuild_package(pkg, &Version::parse(&aur_info.version));

        if needed && already_build.is_some() {
            println!("{} is already built, ignoring", pkg);
        } else {
            println!("Building {}...", pkg);
            cache.build(&aur_info.package_base)?;
        }
    }

    Ok(())
}

fn do_fetch(matches: &clap::ArgMatches<'_>) -> Command {
    let cache = cache()?;

    let package = matches.value_of("PACKAGE").unwrap();
    if let Some(pkg_info) = aur_api::info(&[package])?.get(0) {
        print!("Fetching repository {} from the AUR... ", pkg_info.package_base.green());
        let dir = cache.fetch(&pkg_info.package_base)?;
        println!("done.");
        println!("Repo checked out at: {}", dir.to_string_lossy());
        Ok(())
    } else {
        println!("Didn't get a package back....");
        Ok(()) // Fixme
    }
}

fn do_cache(matches: &clap::ArgMatches<'_>) -> Command {
    match matches.subcommand() {
        ("info", Some(sub_m)) => do_cache_info(sub_m),
        ("clear", Some(sub_m)) => do_cache_clear(sub_m),
        _ => {
            // should never happen
            bail!("No valid subcommand for cache. This should not happen.")
        }
    }
}

fn do_cache_info(_matches: &clap::ArgMatches<'_>) -> Command {
    let cache = cache()?;

    let size = cache.size();

    let format_size = |size: u64| {
        size.file_size(file_size_opts::BINARY).unwrap()
    };

    println!("Packages:       {}", format_size(size.pkg_dir));
    println!("Aur repos:      {}", format_size(size.repo_dir));
    println!("Sources:        {}", format_size(size.source_dir));
    println!("SRCINFO files:  {}", format_size(size.srcinfo_dir));
    println!();
    println!("{}:          {}", "Total".bold().green(), format_size(size.total()).bold());

    Ok(())
}

fn do_cache_clear(_matches: &clap::ArgMatches<'_>) -> Command {
    cache()?.clear()
}

fn print_info(info: &aur_api::Info) {
    let alpm_handle = alpm::Alpm::new().expect("Could not initialize alpm");
    let localdb = alpm_handle
        .get_localdb()
        .expect("Could not open local package db");

    let mut table = Table::new();

    table.add_row(row!("Name", b->info.name));

    table.add_row(row!("Version", info.version));

    let desc = info.description.as_ref().map(|x| x.as_ref()).unwrap_or("(no description)");
    table.add_row(row!("Description", desc));

    if let Some(ref url) = info.url {
        table.add_row(row!("Url", url));
    }

    table.add_row(row!("AurUrl", info.aur_url()));

    match info.maintainer {
        Some(ref maintainer) => table.add_row(row!("Maintainer", maintainer)),
        None => table.add_row(row!("Maintainer", Fgr->"None")),
    };

    table.add_row(row!("Votes", format!("{}", info.num_votes)));

    table.add_row(row!("Popularity", format!("{:.2}%", info.popularity)));

    if localdb.get_pkg(&info.name).is_ok() {
        table.add_row(row!("Installed", Fgg->"Yes"));
    } else {
        table.add_row(row!("Installed", "No"));
    }

    match info.out_of_date {
        None => { table.add_row(row!("OutOfDate", "No")); },
        Some(ref date) => {
            let datestr = date.with_timezone(&chrono::offset::Local)
                .format("Yes, since %Y-%m-%d %H:%M")
                .to_string();
            table.add_row(row!("OutOfDate", Fgr->datestr));
        },
    };

    match info.license {
        None => table.add_row(row!("License", "None")),
        Some(ref license) => table.add_row(row!("License", fmt_vec(license))),
    };

    table.add_row(row!("Keywords", fmt_vec(&info.keywords)));

    table.add_row(row!(
        "LastModified",
        info.last_modified
            .with_timezone(&chrono::offset::Local)
            .format("%Y-%m-%d %H:%M")
            .to_string(),
    ));

    // TODO: Depends can get long, maybe wrap them at some point
    table.add_row(row!(
        "Depends",
        match info.depends {
            None => String::from("None"),
            Some(ref dep) => fmt_vec(dep),
        },
    ));

    table.add_row(row!(
        "MakeDepends",
        match info.make_depends {
            None => String::from("None"),
            Some(ref dep) => fmt_vec(dep),
        },
    ));

    table.add_row(row!(
        "OptDepends",
        match info.opt_depends {
            None => String::from("None"),
            Some(ref dep) => fmt_vec(dep),
        },
    ));

    table.add_row(row!(
        "Conflicts",
        match info.conflicts {
            None => String::from("None"),
            Some(ref dep) => fmt_vec(dep),
        },
    ));

    table.add_row(row!("PackageBase", info.package_base));

    let format = prettytable::format::FormatBuilder::new()
        .column_separator(' ')
        .build();
    table.set_format(format);

    for cell in table.column_iter_mut(0) {
        cell.style(prettytable::Attr::Bold);
    }

    for row in table.row_iter_mut() {
        row.insert_cell(1, cell!(" :"))
    }

    table.print_tty(false);
}

fn fmt_vec(items: &[String]) -> String {
    if items.is_empty() {
        return String::from("None");
    }
    items.join(", ")
}
