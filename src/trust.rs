use std::path::{Path, PathBuf};
use std::fs::{self, File};
use std::io::{Read, Write};
use std::collections::HashMap;

use toml;
use sha1;
use serde::{Serialize, Deserialize};

use crate::utils::Result;

pub struct TrustDB {
    base_dir: PathBuf,
    repos: HashMap<String, Repo>,
}

#[derive(Serialize, Deserialize, PartialEq, Debug)]
pub struct Repo {
    commit: String,
    #[serde(default, skip_serializing_if = "Option::is_none")] patch: Option<sha1::Digest>,
}

pub struct Trust {
    pub commit: String,
    pub patch_file: Option<PathBuf>,
}

impl TrustDB {
    // Load the trust database if it exists or create an empty one.
    //
    // The trust data base is located under the directory `base_dir`.
    //
    // In older versions, a single file `old_file` was used instead. If the
    // `old_file` exists, but the `base_dir` doesn't, the database is
    // automatically migrated.
    pub fn load(base_dir: &Path, old_file: &Path) -> Result<Self> {
        let db_file = base_dir.join("trust.toml");

        // Migration. Remove in a few versions.
        if old_file.exists() && !base_dir.is_dir() {
            fs::create_dir_all(base_dir)?;
            fs::rename(old_file, &db_file)?;
        }

        if base_dir.is_dir() {
            let mut file = File::open(&db_file)?;
            let mut content: Vec<_> = Vec::new();
            file.read_to_end(&mut content)?;
            Ok(TrustDB {
                base_dir: base_dir.to_owned(),
                repos: toml::from_slice(&content)?,
            })
        } else {
            Ok(TrustDB {
                base_dir: base_dir.to_owned(),
                repos: HashMap::new(),
            })
        }
    }

    fn db_file_name(&self) -> PathBuf {
        self.base_dir.join("trust.toml")
    }

    pub fn write(&self) -> Result<()> {
        let filename = self.db_file_name();
        filename.parent().map(fs::create_dir_all);
        let mut file = File::create(filename)?;
        file.write_all(&toml::to_vec(&self.repos)?)?;
        Ok(())
    }

    pub fn trust(&mut self, name: &str, commit: String) {
        self.repos.insert(
            name.to_owned(),
            Repo {
                commit,
                patch: None,
            },
        );
        // ignore errors here, it's not fatal
        // If we couldn't write the new trust, the user would have to review
        // it again, but nothing more.
        let _ = self.write();
    }

    fn patch_dir_name(&self) -> PathBuf {
        self.base_dir.join("patches")
    }

    fn patch_file(&self, pkgbase: &str) -> PathBuf {
        self.patch_dir_name().join(format!("{}.patch", pkgbase))
    }

    fn save_patch<P: AsRef<[u8]>>(&self, pkgbase: &str, patch: P) -> Result<sha1::Digest> {
        let hash = sha1::Sha1::from(patch.as_ref());

        fs::create_dir_all(self.patch_dir_name())?;

        let mut file = File::create(self.patch_file(pkgbase))?;
        file.write_all(patch.as_ref())?;

        Ok(hash.digest())
    }

    pub fn trust_with_patch<P: AsRef<[u8]>>(&mut self, name: &str, commit: String, patch: P) {
        let hash = match self.save_patch(name, patch) {
            Err(_) => return,
            Ok(x) => x,
        };

        self.repos.insert(
            name.to_owned(),
            Repo {
                commit,
                patch: Some(hash),
            },
        );
        let _ = self.write();
    }

    pub fn get_trust(&self, name: &str) -> Option<Trust> {
        self.repos.get(name).map(|x| {
            let patch = self.patch_file(name);
            let hash = match File::open(&patch) {
                Ok(mut file) => {
                    let mut content = Vec::new();
                    file.read_to_end(&mut content)
                        .ok()
                        .map(|_| sha1::Sha1::from(&content).digest())
                }
                Err(_) => None,
            };

            let patch_file = if hash.is_some() && hash == x.patch {
                Some(patch)
            } else {
                None
            };

            Trust {
                commit: x.commit.clone(),
                patch_file,
            }
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::{self, json};
    use sha1;

    #[test]
    fn repo_serialize() {
        let res = serde_json::to_value(Repo {
            commit: "foo".to_string(),
            patch: None,
        }).unwrap();
        assert_eq!(res, json!({"commit": "foo"}));

        let sha1 = sha1::Sha1::from("bar").digest();
        let res = serde_json::to_value(Repo {
            commit: "foo".to_string(),
            patch: Some(sha1),
        }).unwrap();
        assert_eq!(res, json!({"commit": "foo", "patch": sha1.to_string()}));
    }

    #[test]
    fn repo_deserialize() {
        let res: Repo = serde_json::from_str(r#"{"commit": "foo"}"#).unwrap();
        assert_eq!(
            res,
            Repo {
                commit: "foo".to_string(),
                patch: None,
            }
        );

        let sha1 = sha1::Sha1::from("bar").digest();
        let res: Repo = serde_json::from_str(
            r#"{
"commit": "foo",
"patch": "62cdb7020ff920e5aa642c3d4066950dd1f01f4d"
}"#,
        ).unwrap();
        assert_eq!(
            res,
            Repo {
                commit: "foo".to_string(),
                patch: Some(sha1),
            }
        );
    }
}
