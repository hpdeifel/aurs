use std::ffi;
use std::ptr;
use std::marker;
use std::fmt::{Debug, Display};
use std::fmt;
use std::os::raw::c_void;
use std::cmp::Ordering;

use failure::{self, Error};
use alpm_sys::*;

use crate::utils::Result;

pub struct Errno {
    inner: alpm_errno_t,
}

impl Errno {
    pub fn msg(&self) -> String {
        let cstr = unsafe { ffi::CStr::from_ptr(alpm_strerror(self.inner)) };

        cstr.to_string_lossy().into_owned()
    }
}

impl Display for Errno {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.msg())
    }
}

impl Debug for Errno {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{{ Errno {}: {} }}", self.inner as i32, self.msg())
    }
}

impl failure::Fail for Errno {}

pub struct Alpm {
    handle: *mut alpm_handle_t,
}

impl Alpm {
    pub fn new() -> Result<Alpm> {
        // This strdup seems unneccesary and maybe leaks memory
        let root = ffi::CString::new("/")?;
        let db_path = ffi::CString::new("/var/lib/pacman")?;

        let mut err: alpm_errno_t = 0;
        let handle = unsafe { alpm_initialize(root.as_ptr(), db_path.as_ptr(), &mut err) };

        if ptr::eq(handle, ptr::null()) {
            return Err(Error::from(Errno { inner: err }));
        }

        Ok(Alpm { handle })
    }

    fn errno(&self) -> Errno {
        Errno {
            inner: unsafe { alpm_errno(self.handle) },
        }
    }

    fn error(&self) -> Error {
        Error::from(self.errno())
    }

    pub fn get_localdb(&self) -> Result<Database<'_>> {
        let handle = unsafe { alpm_get_localdb(self.handle) };

        if ptr::eq(handle, ptr::null()) {
            return Err(self.errno().into());
        }

        Ok(Database { handle, lib: self })
    }

    pub fn get_syncdbs(&self) -> List<'_, Database<'_>> {
        List::new(unsafe { alpm_get_syncdbs(self.handle) }, self)
    }
}

impl Drop for Alpm {
    fn drop(&mut self) {
        // Ignores the error code:
        //
        // alpm_release returns -1 in case of an error. I don't really know what
        // to do in this case, though.
        unsafe { alpm_release(self.handle) };
    }
}

pub trait FromVoidPtr<'a> {
    fn from_void_ptr(_: *mut c_void, _: &'a Alpm) -> Self;
}

pub struct Database<'a> {
    handle: *mut alpm_db_t,
    lib: &'a Alpm,
}

impl<'a> Database<'a> {
    pub fn new(ptr: *mut alpm_db_t, lib: &'a Alpm) -> Self {
        Database { handle: ptr, lib }
    }

    pub fn get_pkg(&self, name: &str) -> Result<Package<'_>> {
        let cname = ffi::CString::new(name)?;
        let handle = unsafe { alpm_db_get_pkg(self.handle, cname.as_ptr()) };

        if ptr::eq(handle, ptr::null()) {
            return Err(self.lib.error());
        }

        Ok(Package {
            handle,
            _marker: Some(marker::PhantomData),
        })
    }

    pub fn get_pkgcache(&self) -> List<'_, Package<'_>> {
        let handle = unsafe { alpm_db_get_pkgcache(self.handle) };

        List::new(handle, self.lib)
    }

    pub fn find_satisfier(&self, depstring: &str) -> Option<Package<'_>> {
        let pkgcache = unsafe { alpm_db_get_pkgcache(self.handle) };

        let c_depstring =
            ffi::CString::new(depstring).expect("Cannot create CString with 0-Byte(s).");
        let package = unsafe { alpm_find_satisfier(pkgcache, c_depstring.as_ptr()) };

        if package.is_null() {
            None
        } else {
            Some(Package {
                handle: package,
                _marker: Some(marker::PhantomData),
            })
        }
    }
}

impl<'a> FromVoidPtr<'a> for Database<'a> {
    fn from_void_ptr(ptr: *mut c_void, lib: &'a Alpm) -> Self {
        Self::new(ptr as *mut alpm_db_t, lib)
    }
}

pub struct Package<'a> {
    handle: *mut alpm_pkg_t,
    _marker: Option<marker::PhantomData<&'a ()>>,
}

#[derive(PartialEq, Eq)]
pub enum PackageReason {
    Explicit,
    Depend,
}

impl<'a> Package<'a> {
    pub fn name(&self) -> String {
        let cstr = unsafe { ffi::CStr::from_ptr(alpm_pkg_get_name(self.handle)) };

        cstr.to_string_lossy().into_owned()
    }

    pub fn reason(&self) -> PackageReason {
        let reason = unsafe { alpm_pkg_get_reason(self.handle) };
        if reason == _alpm_pkgreason_t_ALPM_PKG_REASON_EXPLICIT {
            PackageReason::Explicit
        } else {
            PackageReason::Depend
        }
    }

    pub fn version(&self) -> String {
        let version = unsafe { ffi::CStr::from_ptr(alpm_pkg_get_version(self.handle)) };

        version.to_string_lossy().into_owned()
    }
}

impl<'a> FromVoidPtr<'a> for Package<'a> {
    fn from_void_ptr(ptr: *mut c_void, _lib: &'a Alpm) -> Self {
        let handle = ptr as *mut alpm_pkg_t;
        Package {
            handle,
            _marker: Some(marker::PhantomData),
        }
    }
}

pub struct List<'a, A> {
    handle: *mut alpm_list_t,
    lib: &'a Alpm,
    _marker: marker::PhantomData<&'a A>,
}

impl<'a, A> List<'a, A> {
    fn new(ptr: *mut alpm_list_t, lib: &'a Alpm) -> Self {
        List {
            handle: ptr,
            lib,
            _marker: marker::PhantomData,
        }
    }
}

impl<'a, A: 'a> Iterator for List<'a, A>
where
    A: FromVoidPtr<'a>,
{
    type Item = A;

    fn next(&mut self) -> Option<A> {
        let old = self.handle;
        if old.is_null() {
            return None;
        }
        self.handle = unsafe { alpm_list_next(self.handle) };
        let data = unsafe { (*old).data };
        Some(A::from_void_ptr(data, self.lib))
    }
}

pub fn vercmp(a: &str, b: &str) -> Ordering {
    let astr = ffi::CString::new(a).expect("Could not allocate c string");
    let bstr = ffi::CString::new(b).expect("Could not allocate c string");

    let res = unsafe { alpm_pkg_vercmp(astr.as_ptr(), bstr.as_ptr()) };

    if res == 0 {
        Ordering::Equal
    } else if res < 0 {
        Ordering::Less
    } else {
        Ordering::Greater
    }
}
