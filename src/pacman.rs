use std::process::Command;
use std::path::{Path, PathBuf};
use std::ffi;
use std::fs::File;

use failure::{ResultExt, err_msg, format_err};

use crate::srcinfo::Version;
use crate::utils::{ask, Result};

pub fn foreign_packages() -> Result<Vec<String>> {
    let output = pacman()
        .arg("--query")
        .arg("--quiet")
        .arg("--foreign")
        .output()
        .context("Failed to execute pacman")?;

    if output.status.success() {
        Ok(String::from_utf8_lossy(&output.stdout)
            .lines()
            .map(String::from)
            .collect())
    } else {
        Err(format_err!("{}", String::from_utf8_lossy(&output.stderr)))
    }
}

pub fn generate_srcinfo(path: &Path) -> Result<String> {
    let output = makepkg(path)
        .arg("--printsrcinfo")
        .arg("--holdver")
        .output()
        .context("Failed to execute makepkg")?;

    if output.status.success() {
        Ok(String::from_utf8_lossy(&output.stdout).into_owned())
    } else {
        Err(format_err!("{}", String::from_utf8_lossy(&output.stderr)))
    }
}

pub fn update_vcs_srcinfo(build: &BuildInfo) -> Result<()> {
    let output = makepkg(&build.pkgbuild_dir)
        .env("SRCDEST", &build.src_dir)
        .env("PKGDEST", &build.pkg_dir)
        .env("BUILDDIR", &build.build_dir)
        .env("PKGEXT", ".pkg.tar.xz")
        .arg("--nobuild")
        .arg("--nodeps")
        .output()
        .context("Failed to execute makepkg")?;

    if output.status.success() {
        Ok(())
    } else {
        Err(format_err!("{}", String::from_utf8_lossy(&output.stderr)))
    }
}

/// Return version of package in pacman repo
pub fn repo_version(pkgname: &str) -> Result<Option<Version>> {
    let output = pacman()
        .arg("--sync")
        .arg("--print")
        .arg("--print-format")
        .arg("%v")
        .arg(pkgname)
        .output()
        .context("Failed to execute pacman")?;

    if output.status.success() {
        let version = String::from_utf8_lossy(&output.stdout);
        Ok(Some(version.trim().into()))
    } else {
        Ok(None)
    }
}

pub fn install_from_repo<T>(pkgs: &[T]) -> Result<()>
where
    T: AsRef<ffi::OsStr>,
{
    loop {
        let output = sudo_pacman()
            .arg("--sync")
            .arg("--needed")
            .arg("--asdeps")
            .args(pkgs)
            .status()
            .context("Failed to execute pacman")?;
        
        if output.success() {
            return Ok(())
        } else {
            match ask(format_args!("\nTry again? (Y/n): "))?.to_lowercase().as_str() {
                "" | "y" | "yes" => continue,
                _ => return Err(err_msg("Pacman failed. User didn't want to continue"))
            }
        }
        
    }
}

pub fn install_pkg_file(file: &Path, asdeps: bool) -> Result<()> {
    // First, try to execute pacman in non-interactive mode. But: If conflicts
    // occur, this will fail and we need to repeat the process without
    // --noconfirm to let the user handle the conflict.

    let mut pacman = sudo_pacman();
    pacman.arg("--upgrade");
    if asdeps {
        pacman.arg("--asdeps");
    };

    let output = pacman
        .arg("--noconfirm")
        .arg(file)
        .output()
        .context("Failed to execute pacman")?;

    if output.status.success() {
        return Ok(())
    } else {
        eprintln!("Pacman failed: {}", String::from_utf8_lossy(&output.stderr));
        match ask(format_args!("\nTry again? (Y/n): "))?.to_lowercase().as_str() {
            "" | "y" | "yes" => {},
            _ => return Err(err_msg("Pacman failed. User didn't want to continue"))
        }
    }

    loop {
        let mut pacman = sudo_pacman();
        pacman.arg("--upgrade");
        if asdeps {
            pacman.arg("--asdeps");
        };

        let output = pacman
            .arg(file)
            .status()
            .context("Failed to execute pacman")?;

        if output.success() {
            return Ok(())
        } else {
            match ask(format_args!("\nTry again? (Y/n): "))?.to_lowercase().as_str() {
                "" | "y" | "yes" => continue,
                _ => return Err(err_msg("Pacman failed. User didn't want to continue"))
            }
        }
    }
}

fn pacman() -> Command {
    Command::new("pacman")
}

fn sudo_pacman() -> Command {
    let mut sudo = Command::new("sudo");
    sudo.arg("pacman");
    sudo
}

pub struct BuildInfo {
    pub pkgbuild_dir: PathBuf,
    pub build_dir: PathBuf,
    pub pkg_dir: PathBuf,
    pub log_dir: PathBuf,
    pub src_dir: PathBuf,
}

pub fn build_package(build: &BuildInfo) -> Result<bool> {
    // create log file
    let stderr = File::create(build.log_dir.join("stderr"))?;
    let stdout = File::create(build.log_dir.join("stdout"))?;

    let res = makepkg(&build.pkgbuild_dir)
        .stderr(stderr)
        .stdout(stdout)
        .arg("--holdver")
        .arg("--nocolor")
        .arg("--clean")
        .env("SRCDEST", &build.src_dir)
        .env("PKGDEST", &build.pkg_dir)
        .env("BUILDDIR", &build.build_dir)
        .env("PKGEXT", ".pkg.tar.xz")
        .status()
        .context("Failed to execute makepkg")?;

    if res.success() {
        Ok(true)
    } else {
        Ok(false)
    }
}

fn makepkg(path: &Path) -> Command {
    let mut command = Command::new("makepkg");
    command.current_dir(path);
    command
}
