use std::fmt;
use std::result;

use url::Url;
use reqwest;
use failure::{ResultExt, format_err};
use failure;
use chrono::{DateTime, TimeZone, Utc};
use chrono::serde::ts_seconds;
use serde::{de, Deserialize};
// use serde_derive::Deserialize;

use crate::utils::Result;

#[allow(dead_code)]
#[derive(Clone, Copy)]
pub enum SearchField {
    Name,
    NameAndDesc,
    Maintainer,
}

impl fmt::Display for SearchField {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match *self {
            SearchField::Name => write!(f, "name"),
            SearchField::NameAndDesc => write!(f, "name-desc"),
            SearchField::Maintainer => write!(f, "maintainer"),
        }
    }
}

#[derive(Deserialize, Debug)]
#[serde(tag = "type")]
enum Response {
    #[serde(rename = "search")]
    Search {
        resultcount: i32,
        results: Vec<SearchResult>,
    },
    #[serde(rename = "multiinfo")] Multiinfo {
        results: Vec<Info>,
    },
    #[serde(rename = "error")] Error {
        error: String,
    },
}

// TODO Maybe remove the duplication between SearchResult and Info

#[derive(Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct SearchResult {
    #[serde(rename = "ID")] pub id: i32,
    pub name: String,
    #[serde(rename = "PackageBaseID")] pub package_base_id: i32,
    pub package_base: String,
    pub version: String,
    pub description: Option<String>,
    #[serde(rename = "URL")] pub url: Option<String>,
    pub num_votes: i32,
    pub popularity: f32,
    #[serde(deserialize_with = "deserialize_opt_date")] pub out_of_date: Option<DateTime<Utc>>,
    pub maintainer: Option<String>,
    #[serde(with = "ts_seconds")] pub first_submitted: DateTime<Utc>,
    #[serde(with = "ts_seconds")] pub last_modified: DateTime<Utc>,
    #[serde(rename = "URLPath")] pub url_path: String,
}

impl SearchResult {
    pub fn is_split_package(&self) -> bool {
        self.name != self.package_base
    }
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "PascalCase")]
pub struct Info {
    #[serde(rename = "ID")] pub id: i32,
    pub name: String,
    #[serde(rename = "PackageBaseID")] pub package_base_id: i32,
    pub package_base: String,
    pub version: String,
    pub description: Option<String>,
    #[serde(rename = "URL")] pub url: Option<String>,
    pub num_votes: i32,
    pub popularity: f32,
    #[serde(deserialize_with = "deserialize_opt_date")] pub out_of_date: Option<DateTime<Utc>>,
    pub maintainer: Option<String>,
    #[serde(with = "ts_seconds")] pub first_submitted: DateTime<Utc>,
    #[serde(with = "ts_seconds")] pub last_modified: DateTime<Utc>,
    #[serde(rename = "URLPath")] pub url_path: String,
    pub depends: Option<Vec<String>>,
    pub make_depends: Option<Vec<String>>,
    pub opt_depends: Option<Vec<String>>,
    pub conflicts: Option<Vec<String>>,
    pub license: Option<Vec<String>>,
    pub keywords: Vec<String>,
}

impl Info {
    pub fn aur_url(&self) -> String {
        format!("https://aur.archlinux.org/packages.php?ID={}", self.id)
    }
}

const AUR_URL: &str = "https://aur.archlinux.org/rpc/";

pub fn search(field: SearchField, keyword: &str) -> Result<Vec<SearchResult>> {
    let search_url = Url::parse_with_params(
        AUR_URL,
        &[
            ("v", "5"),
            ("type", "search"),
            ("by", &field.to_string()),
            ("arg", keyword),
        ],
    )?;

    match reqwest::get(search_url)?
        .json()
        .context("Failed to parse AUR api response")?
    {
        Response::Error { error } => Err(format_err!("Error returned by AUR: {}", error)),
        Response::Multiinfo { .. } => Err(failure::err_msg("Unexpected response from AUR")),
        Response::Search { results, .. } => Ok(results),
    }
}

pub fn info<U>(pkgs: &[U]) -> Result<Vec<Info>>
where
    U: AsRef<str>,
{
    let mut res = Vec::with_capacity(pkgs.len());

    for chunk in pkgs.chunks(50) {
        res.extend(info_impl(chunk)?);
    }

    Ok(res)
}

fn info_impl<U>(pkgs: &[U]) -> Result<Vec<Info>>
where
    U: AsRef<str>,
{
    let mut params = vec![("v", "5"), ("type", "info")];

    params.extend(pkgs.iter().map(|x| ("arg[]", x.as_ref())));

    let info_url = Url::parse_with_params(AUR_URL, params)?;

    match reqwest::get(info_url)?
        .json()
        .context("Failed to parse AUR api response")?
    {
        Response::Error { error } => Err(format_err!("Error returned by AUR: {}", error)),
        Response::Search { .. } => Err(failure::err_msg("Unexpected response from AUR")),
        Response::Multiinfo { results } => Ok(results),
    }
}

pub fn deserialize_opt_date<'de, D>(d: D) -> result::Result<Option<DateTime<Utc>>, D::Error>
where
    D: de::Deserializer<'de>,
{
    Option::<i64>::deserialize(d).map(|optint| optint.map(|int| Utc.timestamp(int, 0)))
}
