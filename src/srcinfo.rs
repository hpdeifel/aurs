use std::collections::HashMap;
use std::cmp::Ordering;
use std::fmt::{self, Display, Formatter};
use std::result;

use lcs_diff::{self, DiffResult};
use failure::{bail, format_err};

use crate::alpm;
use crate::utils::Result;

#[derive(PartialEq, Debug)]
pub struct Srcinfo {
    pkgbase: Section,
    packages: HashMap<String, Section>,
}

#[derive(PartialEq, Debug)]
pub struct Section {
    name: String,
    entries: HashMap<String, Vec<String>>,
}

#[derive(PartialEq)]
pub struct Package {
    pub name: String,
    pub version: Version,
    pub depends: Vec<Dependency>,
    pub makedepends: Vec<Dependency>,
    pub source: Vec<String>,
}

impl Package {
    /// Decide if the package is a VCS package. See section "USING VCS SOURCES"
    /// of `PKGBUILD(5)` for details.
    pub fn is_vcs(&self) -> bool {
        fn vcs_source(s: &str) -> bool {
            let url = if let Some(idx) = s.find("::") {
                &s[idx+2..]
            } else {
                s
            };
            url.starts_with("git") || url.starts_with("bzr") || url.starts_with("hg")
                || url.starts_with("svn")
        };

        self.source.iter().any(|s| vcs_source(s))
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Version {
    pub epoch: Option<String>,
    pub version: String,
    pub release: Option<String>,
}

impl Display for Version {
    fn fmt(&self, f: &mut Formatter<'_>) -> result::Result<(), fmt::Error> {
        if let Some(ref epoch) = self.epoch {
            write!(f, "{}:", epoch)?;
        }
        write!(f, "{}", &self.version)?;
        if let Some(ref rev) = self.release {
            write!(f, "-{}", rev)?;
        }
        Ok(())
    }
}

impl Version {
    pub fn parse(v: &str) -> Self {
        let mut split = v.splitn(2, ':');
        let first = split.next().unwrap();
        let (epoch, rest) = match split.next() {
            None => (None, first),
            Some(r) => if first.chars().all(|c| c.is_digit(10)) {
                (if first.is_empty() { None } else { Some(first) }, r)
            } else {
                (None, v)
            },
        };

        split = rest.splitn(2, '-');
        let version = split.next().unwrap();

        Version {
            epoch: epoch.map(String::from),
            version: version.to_string(),
            release: split.next().map(String::from),
        }
    }

    /// Compare two versions. See the `vercmp(8)` manpage for details.
    ///
    /// **Note** this is not a proper equivalence relation, not even a partial
    /// one due to the special handling of the pkgrel.
    ///
    /// # Examples
    ///
    /// ```
    /// use aurs::srcinfo::Version;
    /// use std::cmp::Ordering;
    /// assert_eq!(Version::compare(&"1".into(), &"2".into()), Ordering::Less);
    /// assert_eq!(Version::compare(&"2".into(), &"1".into()), Ordering::Greater);
    /// assert_eq!(Version::compare(&"2.0-1".into(), &"1.7-6".into()), Ordering::Greater);
    /// assert_eq!(Version::compare(&"2.0".into(), &"2.0-13".into()), Ordering::Equal);
    /// assert_eq!(Version::compare(&"4.34".into(), &"1:001".into()), Ordering::Less);
    /// ```
    pub fn compare(&self, other: &Version) -> Ordering {
        alpm::vercmp(&self.to_string(), &other.to_string())
    }

    /// Create a diff as computed by `lcs_diff`.
    ///
    /// This diffs the individual components: epoch, version split at dots, release.
    ///
    /// # Example
    ///
    /// ```
    /// use aurs::srcinfo::Version;
    /// use lcs_diff::DiffResult::*;
    /// let ver1 = Version::parse("1.1");
    /// let ver2 = Version::parse("1.2");
    /// let diff = ver1.diff(ver2);
    ///
    /// assert_eq!(diff[0], Common("1"))
    /// assert_eq!(diff[1], Common("."))
    /// assert_eq!(diff[2], Removed("1"))
    /// assert_eq!(diff[3], Added("2"))
    /// ```
    pub fn diff<'a>(&'a self, other: &'a Version) -> Vec<DiffResult<&'a str>> {
        let parts1 = self.diff_parts();
        let parts2 = other.diff_parts();

        lcs_diff::diff(&parts1, &parts2)
    }

    fn diff_parts(&self) -> Vec<&str> {
        let mut parts: Vec<&str> = Vec::new();

        if let Some(ref epoch) = self.epoch {
            parts.push(epoch);
            parts.push(":");
        }

        for (i, part) in self.version.split(".").enumerate() {
            if i != 0 {
                parts.push(".");
            }
            parts.push(part);
        }

        if let Some(ref release) = self.release {
            parts.push("-");
            parts.push(release);
        }

        parts
    }
}

impl<'a> From<&'a str> for Version {
    fn from(s: &'a str) -> Self {
        Version::parse(s)
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct Dependency {
    pub name: String,
    pub version: Option<VersionRequirement>,
}

impl Dependency {
    pub fn parse(input: &str) -> Self {
        let cmp;
        let mut iter;

        if input.contains(">=") {
            cmp = VersionComparator::GreaterOrEqual;
            iter = input.split(">=");
        } else if input.contains("<=") {
            cmp = VersionComparator::LessOrEqual;
            iter = input.split("<=");
        } else if input.contains('>') {
            cmp = VersionComparator::Greater;
            iter = input.split(">");
        } else if input.contains('<') {
            cmp = VersionComparator::Less;
            iter = input.split("<");
        } else if input.contains('=') {
            cmp = VersionComparator::Equal;
            iter = input.split("=");
        } else {
            return Dependency {
                name: input.to_string(),
                version: None,
            };
        }

        let name = iter.next().unwrap().to_string();
        let version = iter.next().unwrap().to_string();
        Dependency {
            name,
            version: Some(VersionRequirement {
                comparision: cmp,
                version,
            }),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub struct VersionRequirement {
    pub version: String,
    pub comparision: VersionComparator,
}

#[derive(PartialEq, Debug, Clone)]
pub enum VersionComparator {
    GreaterOrEqual, // >=
    LessOrEqual,    // <=
    Equal,          // =
    Greater,        // >
    Less,           // <
}

impl Srcinfo {
    pub fn parse(input: &str) -> Result<Self> {
        let lines = input
            .lines()
            .filter(|x| !is_whitespace(x))
            .map(|x| x.splitn(2, '=').map(|x| x.trim()));

        let mut packages: Vec<Section> = Vec::new();
        let mut pkgbase: Option<Section> = None;

        let mut current_section: Option<Section> = None;
        let mut is_pkgbase = false;

        for mut assignment in lines {
            // key must be there, it's even there if the line has no equal sign
            // and empty lines are discarded beforehand.
            let key = assignment.next().unwrap();
            let value = match assignment.next() {
                Some(x) => x,
                None => bail!("No equal sign in line"),
            };

            match key {
                "pkgbase" => {
                    if pkgbase.is_some() || is_pkgbase {
                        bail!("Two pkgbase sections");
                    }
                    if let Some(sec) = current_section {
                        packages.push(sec);
                    }
                    current_section = Some(Section::new(value.to_string()));
                    is_pkgbase = true;
                }
                "pkgname" => {
                    if let Some(sec) = current_section {
                        if is_pkgbase {
                            pkgbase = Some(sec)
                        } else {
                            packages.push(sec);
                        }
                    }
                    is_pkgbase = false;
                    current_section = Some(Section::new(value.to_string()));
                }
                _ => match current_section {
                    None => bail!("Assignments without section header"),
                    Some(ref mut sec) => {
                        sec.entries
                            .entry(key.to_string())
                            .or_insert_with(Vec::new)
                            .push(value.to_string());
                    }
                },
            }
        }

        match current_section {
            None => {} // TODO Error
            Some(sec) => {
                if is_pkgbase {
                    pkgbase = Some(sec);
                } else {
                    packages.push(sec);
                }
            }
        }

        match pkgbase {
            None => {
                bail!("No pkgbase section");
            } // TODO Error
            Some(sec) => {
                let mut pkgs = HashMap::new();
                for pkg in packages {
                    if pkgs.contains_key(&pkg.name) {
                        bail!("Duplicate package definition in SRCINFO");
                    }
                    pkgs.insert(pkg.name.clone(), pkg);
                }
                Ok(Srcinfo {
                    pkgbase: sec,
                    packages: pkgs,
                })
            }
        }
    }

    pub fn package_names(&self) -> Vec<&str> {
        self.packages.keys().map(|x| x.as_ref()).collect()
    }

    pub fn merge_package(&self, pkgname: &str) -> Option<HashMap<&str, Vec<&str>>> {
        self.packages.get(pkgname).map(|section| {
            let mut map: HashMap<&str, Vec<&str>> = HashMap::new();
            map.extend(
                section
                    .entries
                    .iter()
                    .map(|(x, y)| (x.as_ref(), y.iter().map(|x| x.as_ref()).collect())),
            );
            for (k, v) in &self.pkgbase.entries {
                // only insert if not already present.
                map.entry(k.as_ref())
                    .or_insert_with(|| v.iter().map(String::as_ref).collect());
            }
            map
        })
    }

    pub fn package(&self, pkgname: &str) -> Result<Package> {
        let pkg = self.merge_package(pkgname)
            .ok_or_else(|| format_err!("package {} not found in SRCINFO", pkgname))?;

        let pkgver = get_err1(&pkg, "pkgver")?;
        let epoch = get_err_maybe(&pkg, "epoch")?;
        let pkgrel = get_err_maybe(&pkg, "pkgrel")?;

        let version = Version {
            version: pkgver.to_string(),
            epoch: epoch.map(String::from),
            release: pkgrel.map(String::from),
        };

        let mut depend_strings = Vec::new();
        key_to_vec(&pkg, "depends", &mut depend_strings);
        // Hardcoding x86_64 for the meantime
        key_to_vec(&pkg, "depends_x86_64", &mut depend_strings);

        let depends = depend_strings
            .iter()
            .map(|x| Dependency::parse(x))
            .collect();

        let mut makedepend_strings = Vec::new();
        key_to_vec(&pkg, "makedepends", &mut makedepend_strings);
        key_to_vec(&pkg, "makedepends_x86_64", &mut makedepend_strings);

        let makedepends = makedepend_strings
            .iter()
            .map(|x| Dependency::parse(x))
            .collect();

        let mut source = Vec::new();
        key_to_vec(&pkg, "source", &mut source);
        key_to_vec(&pkg, "source_x86_64", &mut source);

        Ok(Package {
            name: pkgname.to_string(),
            version,
            depends,
            makedepends,
            source,
        })
    }

    pub fn packages(&self) -> Result<HashMap<String, Package>> {
        self.package_names()
            .into_iter()
            .map(|pkg| Ok((pkg.to_string(), self.package(pkg)?)))
            .collect()
    }
}

type Pkgmap<'a> = &'a HashMap<&'a str, Vec<&'a str>>;

fn get_err_maybe<'a>(map: Pkgmap<'a>, key: &str) -> Result<Option<&'a str>> {
    let res = match map.get(key) {
        Some(x) => x,
        None => return Ok(None),
    };

    if res.len() != 1 {
        bail!("key {} has too many entries in SRCINFO", key);
    }

    Ok(Some(res[0]))
}

fn get_err1<'a>(map: Pkgmap<'a>, key: &str) -> Result<&'a str> {
    let res = map.get(key)
        .ok_or_else(|| format_err!("key {} not found in SRCINFO", key))?;

    if res.len() != 1 {
        bail!("key {} has too many entries in SRCINFO", key);
    }

    Ok(res[0])
}

fn key_to_vec(map: &HashMap<&str, Vec<&str>>, key: &str, vec: &mut Vec<String>) {
    if let Some(k) = map.get(key) {
        vec.extend(k.iter().map(|x| x.to_string()));
    }
}

impl Section {
    fn new(name: String) -> Self {
        Section {
            name,
            entries: HashMap::new(),
        }
    }
}

fn is_whitespace(s: &str) -> bool {
    let trimmed = s.trim();
    trimmed.is_empty() || trimmed.starts_with('#')
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn minimal() {
        let input = "pkgbase = test\n";
        let res = Srcinfo::parse(input).unwrap();
        assert_eq!(
            res,
            Srcinfo {
                pkgbase: Section::new("test".to_string()),
                packages: HashMap::new(),
            }
        )
    }

    #[test]
    fn example() {
        let input = "pkgbase = test
a = b
c = d

# a comment
pkgname = foo
e = f";
        let mut pkgs = HashMap::new();
        pkgs.insert(
            String::from("foo"),
            Section {
                name: "foo".to_string(),
                entries: [("e", "f")]
                    .iter()
                    .map(|&(x, y)| (x.to_string(), vec![y.to_string()]))
                    .collect(),
            },
        );
        let srcinfo = Srcinfo {
            pkgbase: Section {
                name: "test".to_string(),
                entries: [("a", "b"), ("c", "d")]
                    .iter()
                    .map(|&(x, y)| (x.to_string(), vec![y.to_string()]))
                    .collect(),
            },
            packages: pkgs,
        };

        assert_eq!(Srcinfo::parse(input).unwrap(), srcinfo);
    }

    #[test]
    fn empty() {
        assert!(Srcinfo::parse("").is_err());
    }

    #[test]
    fn no_section() {
        assert!(Srcinfo::parse("foo = bar\npkgbase = foo").is_err());
    }

    #[test]
    fn two_pkgbase() {
        assert!(Srcinfo::parse("pkgbase = foo\npkgbase = bar").is_err());
        assert!(Srcinfo::parse("pkgbase = foo\npkgname=foobar\npkgbase = bar").is_err());
    }

    #[test]
    fn key_without_value() {
        assert!(Srcinfo::parse("pkgbase = foo\nbar").is_err());
    }

    #[test]
    fn duplicate_keys() {
        let input = "pkgbase = foo\nx = y\nx = z";
        let result = Srcinfo {
            pkgbase: Section {
                name: String::from("foo"),
                entries: [
                    (
                        String::from("x"),
                        vec![String::from("y"), String::from("z")],
                    ),
                ].iter()
                    .cloned()
                    .collect(),
            },
            packages: HashMap::new(),
        };
        assert_eq!(Srcinfo::parse(input).unwrap(), result);
    }

    #[test]
    fn duplicate_packages() {
        let input = "pkgbase = foo\npkgname=foobar\npkgname=foobar";
        assert!(Srcinfo::parse(input).is_err());
    }

    #[test]
    fn package_merging() {
        let src = Srcinfo::parse(
            "pkgbase = test
x = a
pkgname = bar
y = a
pkgname = baz
x = b",
        ).unwrap();

        assert!(src.merge_package("foobar").is_none());
        let merge_pkg = src.merge_package("bar").unwrap();
        assert_eq!(merge_pkg.get("x").unwrap(), &vec!["a"]);

        let merge_pkg = src.merge_package("baz").unwrap();
        assert_eq!(merge_pkg.get("x").unwrap(), &vec!["b"]);
    }

    #[test]
    fn version_good() {
        let src1 = Srcinfo::parse(
            "pkgbase = test
pkgver = 5.2
pkgname = test",
        ).unwrap();

        assert_eq!(
            src1.package("test").unwrap().version,
            Version {
                epoch: None,
                release: None,
                version: "5.2".to_string(),
            }
        );

        let src2 = Srcinfo::parse(
            "pkgbase = test
pkgver = 5.2
pkgrel = 1
pkgname = test",
        ).unwrap();

        assert_eq!(
            src2.package("test").unwrap().version,
            Version {
                epoch: None,
                release: Some("1".to_string()),
                version: "5.2".to_string(),
            }
        );

        let src3 = Srcinfo::parse(
            "pkgbase = test
pkgver = 5.2
pkgrel = 1
epoch = 5
pkgname = test",
        ).unwrap();

        assert_eq!(
            src3.package("test").unwrap().version,
            Version {
                epoch: Some("5".to_string()),
                release: Some("1".to_string()),
                version: "5.2".to_string(),
            }
        );
    }

    #[test]
    fn version_bad_none() {
        let src = Srcinfo::parse(
            "pkgbase = test
pkgname = test",
        ).unwrap();

        assert!(src.package("test").is_err());
    }

    #[test]
    fn version_bad_twice() {
        let src1 = Srcinfo::parse(
            "pkgbase = test
pkgver = 5.2
pkgver = 5.2
pkgname = test",
        ).unwrap();

        assert!(src1.package("test").is_err());

        let src2 = Srcinfo::parse(
            "pkgbase = test
pkgver = 5.2
pkgrel = 1
pkgrel = 1
pkgname = test",
        ).unwrap();

        assert!(src2.package("test").is_err());

        let src3 = Srcinfo::parse(
            "pkgbase = test
pkgver = 5.2
pkgrel = 1
epoch = 5
epoch = 5
pkgname = test",
        ).unwrap();

        assert!(src3.package("test").is_err());
    }

    #[test]
    fn dep_parse() {
        assert_eq!(
            Dependency::parse("foo"),
            Dependency {
                name: "foo".to_string(),
                version: None,
            }
        );
        fn mk(comp: VersionComparator) -> Dependency {
            Dependency {
                name: "foo".to_string(),
                version: Some(VersionRequirement {
                    version: "1".to_string(),
                    comparision: comp,
                }),
            }
        }
        use super::VersionComparator::*;
        assert_eq!(Dependency::parse("foo>1"), mk(Greater));
        assert_eq!(Dependency::parse("foo>=1"), mk(GreaterOrEqual));
        assert_eq!(Dependency::parse("foo<=1"), mk(LessOrEqual));
        assert_eq!(Dependency::parse("foo<1"), mk(Less));
        assert_eq!(Dependency::parse("foo=1"), mk(Equal));
    }

    #[test]
    fn version_parse() {
        assert_eq!(
            Version::parse("1.2"),
            Version {
                epoch: None,
                version: "1.2".to_string(),
                release: None,
            }
        );
        assert_eq!(
            Version::parse("1:1.2"),
            Version {
                epoch: Some("1".to_string()),
                version: "1.2".to_string(),
                release: None,
            }
        );
        assert_eq!(
            Version::parse("1:1.2-r5"),
            Version {
                epoch: Some("1".to_string()),
                version: "1.2".to_string(),
                release: Some("r5".to_string()),
            }
        );
        assert_eq!(
            Version::parse("x:1.2"),
            Version {
                epoch: None,
                version: "x:1.2".to_string(),
                release: None,
            }
        );
        assert_eq!(
            Version::parse(":1.2"),
            Version {
                epoch: None,
                version: "1.2".to_string(),
                release: None,
            }
        );
        assert_eq!(
            Version::parse("1:-5"),
            Version {
                epoch: Some("1".to_string()),
                version: String::new(),
                release: Some("5".to_string()),
            }
        );
    }

    #[test]
    fn vcs_package() {
        let mut pkg = Package {
            name: String::new(),
            version: Version::parse("2.0"),
            depends: Vec::new(),
            makedepends: Vec::new(),
            source: Vec::new(),
        };
        assert!(!pkg.is_vcs());
        pkg.source = ["git://example.com"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        assert!(pkg.is_vcs());
        pkg.source = ["git+http://example.com"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        assert!(pkg.is_vcs());
        pkg.source = ["hg+http://example.com"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        assert!(pkg.is_vcs());
        pkg.source = ["svn+http://example.com"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        assert!(pkg.is_vcs());
        pkg.source = ["bzr+http://example.com"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        assert!(pkg.is_vcs());
        pkg.source = ["directory::git+http://example.com"]
            .iter()
            .map(|x| x.to_string())
            .collect();
        assert!(pkg.is_vcs());
    }
}
