use std::path::{Path, PathBuf};
use std::fs;
use std::process::{Command, Stdio};
use std::io::{self, BufRead, Read, Write};
use std::collections::HashMap;
use std::cell::RefCell;

use failure::{ResultExt, bail, format_err};
use url::Url;
use tempfile::TempDir;
use colored::Colorize;
use walkdir::WalkDir;

use crate::trust::TrustDB;
use crate::pacman;
use crate::srcinfo::{self, Srcinfo, Version};
use crate::utils::{ask_choice, Choice, Result};
use crate::output::spinner;

pub struct Cache {
    repo_dir: PathBuf,
    source_dir: PathBuf,
    pkg_dir: PathBuf,
    srcinfo_dir: PathBuf,
    trust_db: TrustDB,
    volatile_pkg_cache: Option<RefCell<Vec<String>>>,
}

impl Cache {
    pub fn make(path: &Path, trust_db: TrustDB) -> Result<Self> {
        let repo_dir = [path, Path::new("repos")].iter().collect();
        fs::create_dir_all(&repo_dir)?;
        let source_dir = [path, Path::new("sources")].iter().collect();
        fs::create_dir_all(&source_dir)?;
        let pkg_dir = [path, Path::new("packages")].iter().collect();
        fs::create_dir_all(&pkg_dir)?;
        let srcinfo_dir = [path, Path::new("srcinfos")].iter().collect();
        fs::create_dir_all(&srcinfo_dir)?;

        Ok(Cache {
            repo_dir,
            source_dir,
            pkg_dir,
            srcinfo_dir,
            trust_db,
            volatile_pkg_cache: None,
        })
    }

    pub fn make_pkgcache_volatile(&mut self) {
        if self.volatile_pkg_cache.is_none() {
            self.volatile_pkg_cache = Some(RefCell::new(Vec::new()));
        }
    }

    fn pkg_dir(&self, pkgname: &str) -> PathBuf {
        let mut path = self.repo_dir.clone();
        path.push(pkgname.to_string() + ".git");
        path
    }

    /// Fetches the repo named `pkgbase` from the AUR with git.
    ///
    /// If this repo is already checked out in the cache, `fetch` just updates
    /// it to the latest upstream master, and creates a fresh clone otherwise.
    ///
    /// This doesn't do any diff/trust checking. Use `fetch_and_trust` for that.
    pub fn fetch(&self, pkgbase: &str) -> Result<PathBuf> {
        let dir = self.pkg_dir(pkgbase);
        let url = clone_url(pkgbase);

        if dir.exists() {
            Git::with_root(&dir).fetch("origin")?;
            Git::with_root(&dir).reset_hard("origin/master")?;
        } else {
            git().clone(&url, &dir)?;
        }

        Ok(dir.to_path_buf())
    }

    pub fn fetch_and_trust(&mut self, pkgbase: &str) -> Result<PathBuf> {
        let dir = self.fetch(pkgbase)?;

        let current_commit = Git::with_root(&dir).current_commit()?;

        let trust = self.trust_db.get_trust(pkgbase);
        let trusted_commit = trust.as_ref().map(|x| x.commit.as_str());
        let patch_file = trust.as_ref().and_then(|x| x.patch_file.as_ref());

        // First thing in the morning: Apply the saved patch
        if let Some(patch) = patch_file {
            if !(Git::with_root(&dir).apply(patch)?) {
                spinner::inhibit();
                eprintln!("Saved patch failed to apply for repo {}", pkgbase.bold());
            }
        }

        // don't ask user if current commit is already trusted
        if trusted_commit == Some(&current_commit) {
            return Ok(dir.to_path_buf());
        }

        if !review(pkgbase, &dir, trusted_commit)? {
            bail!("Repo not trusted, aborting.");
        }

        match Git::with_root(&dir).get_patch(&current_commit)? {
            //  Repo modified, save new patch
            Some(patch) => self.trust_db
                .trust_with_patch(pkgbase, current_commit, patch),
            // Unmodified, trust as is
            None => self.trust_db.trust(pkgbase, current_commit),
        }

        Ok(dir.to_path_buf())
    }

    fn srcinfo_filename(&self, pkgbase: &str) -> PathBuf {
        self.srcinfo_dir.join(pkgbase)
    }

    /// Return the content of the cached srcinfo file for a specific package and
    /// commit.
    ///
    /// If either the package has no cached srcinfo or the cached commit is
    /// different, return `None`.
    ///
    /// Errors are returned when the file exists but is not readable in the way
    /// that we expect.
    fn cached_srcinfo(&self, pkgbase: &str, commit: &str) -> Result<Option<String>> {
        let file = match fs::File::open(self.srcinfo_filename(pkgbase)) {
            Err(_) => return Ok(None),
            Ok(res) => res
        };

        let mut buf_reader = io::BufReader::new(file);
        let mut cached_commit = String::new();
        buf_reader.read_line(&mut cached_commit)?;
        if commit != cached_commit.trim() {
            return Ok(None);
        }

        let mut content = String::new();
        buf_reader.read_to_string(&mut content)?;

        Ok(Some(content))
    }

    fn write_srcinfo_to_cache(&self, pkgbase: &str, commit: &str, srcinfo: &str) -> Result<()> {
        let mut file = fs::File::create(self.srcinfo_filename(pkgbase))?;

        writeln!(file, "{}", commit)?;
        file.write(srcinfo.as_bytes())?;

        Ok(())
    }

    pub fn get_srcinfo(
        &self,
        pkgbase: &str,
        update_vcs: bool,
    ) -> Result<HashMap<String, srcinfo::Package>> {
        // TODO For "update_vcs": Run makepkg --no-build beforehand
        let path = self.pkg_dir(pkgbase);
        let commit = Git::with_root(&path).current_commit()?;

        let srcinfo_content = match self.cached_srcinfo(pkgbase, &commit)? {
            Some(res) => res,
            None => {
                let content = pacman::generate_srcinfo(&path)?;
                self.write_srcinfo_to_cache(pkgbase, &commit, &content)?;
                content
            },
        };

        let srcinfo = Srcinfo::parse(&srcinfo_content)
            .and_then(|src| src.packages())?;

        if update_vcs && srcinfo.values().next().unwrap().is_vcs() {
            let (_temp_dir, buildinfo) = self.make_buildinfo(pkgbase)?;
            // if vcs, get real version number
            pacman::update_vcs_srcinfo(&buildinfo)?;
            // and get the updated srcinfo
            pacman::generate_srcinfo(&path)
                .and_then(|txt| Srcinfo::parse(&txt))
                .and_then(|src| src.packages())
        } else {
            Ok(srcinfo)
        }
    }

    pub fn ensure_pkg_source_dir(&self, pkgbase: &str) -> Result<PathBuf> {
        let dir = [&self.source_dir, Path::new(pkgbase)].iter().collect();
        fs::create_dir_all(&dir)?;
        Ok(dir)
    }

    pub fn prebuild_package(&self, pkgname: &str, version: &Version) -> Option<PathBuf> {
        // Assume .xz extionsion and hardcode arch for the time being
        let arch_filename = format!("{}-{}-x86_64.pkg.tar.xz", pkgname, version);
        let any_filename = format!("{}-{}-any.pkg.tar.xz", pkgname, version);
        let arch_path: PathBuf = [&self.pkg_dir, Path::new(&arch_filename)].iter().collect();
        let any_path: PathBuf = [&self.pkg_dir, Path::new(&any_filename)].iter().collect();

        if let Some(ref cached_pkgs) = self.volatile_pkg_cache {
            if cached_pkgs.borrow().contains(&arch_filename) && arch_path.exists() {
                return Some(arch_path);
            }
            if cached_pkgs.borrow().contains(&any_filename) && any_path.exists() {
                return Some(any_path);
            }
        } else {
            if arch_path.exists() {
                return Some(arch_path);
            }
            if any_path.exists() {
                return Some(any_path);
            }
        }

        None
    }

    /// Returns a TempDir to prevent it from being deleted
    fn make_buildinfo(&self, pkgbase: &str) -> Result<(TempDir, pacman::BuildInfo)> {
        let path = self.pkg_dir(pkgbase);
        let sources = self.ensure_pkg_source_dir(pkgbase)?;

        let tmp_dir = TempDir::new()?;
        let log_dir = tmp_dir.path().join("logs");
        let build_dir = tmp_dir.path().join("build");
        let pkg_dir = tmp_dir.path().join("pkgs");

        for d in &[&log_dir, &build_dir, &pkg_dir] {
            fs::create_dir(d)?;
        }

        Ok((
            tmp_dir,
            pacman::BuildInfo {
                pkgbuild_dir: path,
                build_dir: build_dir,
                pkg_dir: pkg_dir,
                log_dir: log_dir,
                src_dir: sources,
            },
        ))
    }

    pub fn build(&self, pkgbase: &str) -> Result<()> {
        let (_tmp_dir, buildinfo) = self.make_buildinfo(pkgbase)
            .context("Failed to create build directories")?;
        let res = pacman::build_package(&buildinfo).context("Failed to build package")?;

        // TODO: Copy logs to some log cache
        if !res {
            // Write makepkg output to stderr in case of error
            let mut stderr = fs::File::open(buildinfo.log_dir.join("stderr"))
                .context("Failed to open error output of makepkg")?;
            io::copy(&mut stderr, &mut io::stderr())?;
            bail!("makepkg failed");
        }

        // Copy packages to pkgcache
        for pkgfile in fs::read_dir(buildinfo.pkg_dir)? {
            let path = pkgfile?.path();
            let file_name = path.file_name().unwrap();
            fs::copy(&path, self.pkg_dir.join(file_name))?;
            if let Some(ref vol_cache) = self.volatile_pkg_cache {
                vol_cache.borrow_mut().push(file_name.to_string_lossy().into_owned());
            }
        }

        Ok(())
    }

    pub fn size(&self) -> CacheSize {
        CacheSize {
            repo_dir: directory_size(&self.repo_dir),
            source_dir: directory_size(&self.source_dir),
            pkg_dir: directory_size(&self.pkg_dir),
            srcinfo_dir: directory_size(&self.srcinfo_dir),
        }
    }

    /// Remove all cache entries
    pub fn clear(&self) -> Result<()> {
        fs::remove_dir_all(&self.repo_dir)?;
        fs::remove_dir_all(&self.source_dir)?;
        fs::remove_dir_all(&self.pkg_dir)?;
        fs::remove_dir_all(&self.srcinfo_dir)?;

        Ok(())
    }
}

fn directory_size(path: &Path) -> u64 {
    WalkDir::new(path).into_iter()
        .map(|entry| entry.and_then(|x| x.metadata()).map(|x| x.len()))
        .filter_map(|entry| match entry {
            Err(e) => { eprintln!("Error: {}", e); None },
            Ok(res) => Some(res),
        })
        .sum()
}

pub struct CacheSize {
    pub repo_dir: u64,
    pub source_dir: u64,
    pub pkg_dir: u64,
    pub srcinfo_dir: u64,
}

impl CacheSize {
    pub fn total(&self) -> u64 {
        self.repo_dir + self.source_dir + self.pkg_dir + self.srcinfo_dir
    }
}

fn clone_url(pkgname: &str) -> Url {
    Url::parse(&format!("https://aur.archlinux.org/{}.git", pkgname))
        .expect("Failed to parse git url")
}

trait CommandExt {
    fn hope_for_success(&mut self) -> Result<()>;
}

impl CommandExt for Command {
    fn hope_for_success(&mut self) -> Result<()> {
        let res = self.output()?;

        if !res.status.success() {
            bail!("{}", String::from_utf8_lossy(&res.stderr));
        }

        Ok(())
    }
}

fn review(pkgname: &str, dir: &Path, trusted_commit: Option<&str>) -> Result<bool> {
    if let Some(commit) = trusted_commit {
        spinner::inhibit();
        if Git::with_root(dir).diff(commit)? == false {
            // diff empty, so we know we can trust this version
            println!("{}: Diff empty, trusting automatically", pkgname.bold());
            return Ok(true);
        }
        loop {
            println!();
            let answer = ask_choice(
                format_args!(
                    "Trust this diff of {}?",
                    pkgname.green().bold()
                ),
                &[
                    Choice::new("[Y]es", "Trust this diff"),
                    Choice::new("[n]o", "Don't trust this diff and exit immediatly"),
                    Choice::new("[r]eview", "Open file manager to inspect PKGBUILD directory"),
                    Choice::new("[d]iff", "Show diff of the changes since last trusted commit"),
                    Choice::new("[l]og", "Show log since last trusted commit"),
                ],
                'y',
                'n',
            )?.to_ascii_lowercase();

            match answer {
                'y' => {
                    return Ok(true);
                }
                'r' => break, // fall through to file manager handling
                'l' => {
                    println!();
                    Git::with_root(dir).log_diff(commit)?;
                }
                'd' => {
                    println!();
                    Git::with_root(dir).diff(commit)?;
                }
                _ => {
                    return Ok(false);
                }
            }
        }
    }

    spinner::inhibit();

    Command::new("ranger")
        .stdout(Stdio::inherit())
        .stdin(Stdio::inherit())
        .arg(dir)
        .hope_for_success()?;

    let answer = ask_choice(
        format_args!(
            "Trust this PKGBUILD for {}?",
            pkgname.green().bold()
        ),
        &[
            Choice::new("[Y]es", "Trust this PKGBUILD"),
            Choice::new("[n]o", "Don't trust this PKGBUILD and exit immediatly"),
        ],
        'y',
        'n'
    )?.to_ascii_lowercase();

    match answer {
        'y' => Ok(true),
        _ => Ok(false),
    }
}

struct Git<'a> {
    root: Option<&'a Path>,
}

impl<'a> Git<'a> {
    fn with_root(dir: &'a Path) -> Self {
        Git { root: Some(dir) }
    }

    fn command(self) -> Command {
        let mut cmd = Command::new("git");

        if let Some(dir) = self.root {
            cmd.arg("-C").arg(dir);
        }

        cmd
    }

    fn clone(self, url: &Url, target: &Path) -> Result<()> {
        self.command()
            .arg("clone")
            .arg(url.as_ref())
            .arg(target)
            .hope_for_success()
    }

    fn fetch(self, remote: &str) -> Result<()> {
        self.command().arg("fetch").arg(remote).hope_for_success()
    }

    fn reset_hard(self, commit: &str) -> Result<()> {
        self.command()
            .arg("reset")
            .arg("--hard")
            .arg(commit)
            .hope_for_success()
    }

    fn current_commit(self) -> Result<String> {
        let output = self.command().args(&["rev-parse", "HEAD"]).output()?;

        if output.status.success() {
            let commit = String::from_utf8_lossy(&output.stdout).trim().to_string();
            Ok(commit)
        } else {
            bail!("{}", String::from_utf8_lossy(&output.stderr));
        }
    }

    /// Show a diff between `commit` and the working tree.
    ///
    /// Returns `true` if there were differences and `false` otherwise.
    fn diff(self, commit: &str) -> Result<bool> {
        // We exclude .SRCINFO from the diff because we don't use it anyway and
        // it just clutters the review. It's possible that the PKGBUILD tries to
        // use the SRCINFO for something nasty, but stuff like this should be
        // catched when reviewing the PKGBUILD file.

        let status = self.command()
            .arg("--no-pager")
            .arg("diff")
            .arg("--exit-code")
            .arg(commit)
            .arg("--")
            .arg(":(exclude).SRCINFO")
            .status()?;

        if status.code() == Some(0) {
            // Return false (no differences) only if `git diff` returns 0 and
            // wasn't killed by a signal.
            return Ok(false);
        } else {
            return Ok(true);
        }
    }

    fn log_diff(self, commit: &str) -> Result<()> {
        println!("git log --patch {}", commit.to_string() + "..");
        let status = self.command()
            .arg("log")
            .arg("--patch")
            .arg(commit.to_string() + "..")
            .status()?;

        if status.success() {
            Ok(())
        } else {
            Err(format_err!("git-log failed"))
        }
    }

    /// Create a diff between `commit` and the working tree.
    ///
    /// Returns the content (in bytes) of the resulting patch file or `None` if
    /// there are no differences.
    fn get_patch(self, commit: &str) -> Result<Option<Vec<u8>>> {
        let res = self.command().arg("diff").arg(commit).output()?;

        if !res.status.success() {
            bail!("git failed: {}", String::from_utf8_lossy(&res.stderr));
        }

        if res.stdout.is_empty() {
            Ok(None)
        } else {
            Ok(Some(res.stdout))
        }
    }

    fn apply(self, patch_file: &Path) -> Result<bool> {
        let status = self.command().arg("apply").arg(patch_file).status()?;

        Ok(status.success())
    }
}

fn git<'a>() -> Git<'a> {
    Git { root: None }
}
