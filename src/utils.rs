//! Random utilities that have no other place.

use std::fmt::{self, Display};
use std::io::{self, Write};
use std::{result, process};

use colored::{self, ColoredString, Colorize};
use lcs_diff::DiffResult;
use failure::{Error, format_err};
use termion::{raw::IntoRawMode, input::TermRead};
use prettytable::{Table, self, cell};

/// Alias for `Result` with [`failure::Error`][error] as Error type.
///
/// [error]: ../../failure/struct.Error.html
pub type Result<T> = result::Result<T, Error>;

/// Ask the user a question on stdout and return their answer
pub fn ask(args: fmt::Arguments<'_>) -> Result<String> {
    print!("{}", args);
    io::stdout().flush()?;

    let mut input = String::new();
    io::stdin().read_line(&mut input)?;
    Ok(input.trim().to_string())
}

pub struct Choice {
    name: String,
    key_index: usize,
    description: String,
}

impl Choice {
    pub fn new(name: &str, description: &str) -> Self {
        let left_bracket = name.find('[').unwrap();
        let right_bracket = name.find(']').unwrap();

        let plain = format!(
            "{}{}{}",
            &name[..left_bracket],
            name.chars().nth(left_bracket+1).unwrap(),
            &name[right_bracket+1..],
        );

        Choice {
            name: plain,
            key_index: left_bracket,
            description: description.to_string(),
        }
    }

    fn key(&self) -> char {
        self.name.chars().nth(self.key_index).unwrap().to_ascii_lowercase()
    }
}

impl Display for Choice {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let s: &str = &self.name;
        let key = s.chars().nth(self.key_index).unwrap().to_string();
        write!(f, "{}{}{}", &s[0..self.key_index], key.bold(), &s[self.key_index+1..])
    }
}

pub fn ask_choice(question: fmt::Arguments<'_>, options: &[Choice], default: char, abort: char) -> Result<char> {
    'outer: loop {
        let mut stdout = io::stdout().into_raw_mode()?;

        write!(stdout, "{} (", question)?;
        for opt in options {
            write!(stdout, "{}/", opt)?;
        }
        write!(stdout, "{}): ", "?".bold())?;
        io::stdout().flush()?;

        for next_key in io::stdin().keys() {
            let input = next_key?;

            use termion::event::Key::*;

            match input {
                Char('\n') => {
                    print!("\r\n");
                    return Ok(default);
                }
                Char('?') => {
                    drop(stdout); // allow to use \n for newlines
                    let mut table = Table::new();
                    for c in options {
                        let mut row = prettytable::Row::empty();
                        row.add_cell(cell!(format!("  {}  ", c.key().to_string())));
                        row.add_cell(cell!(c.description.clone()));
                        table.add_row(row);
                    }
                    println!("\nPossible Answers:\n");
                    let format = prettytable::format::FormatBuilder::new()
                        .column_separator(' ')
                        .build();
                    table.set_format(format);
                    table.print_tty(false);
                    println!();
                    continue 'outer;
                }
                Char(x) => {
                    print!("{}\r\n", x);
                    let lower = x.to_ascii_lowercase();
                    if options.iter().find(|c| c.key() == lower).is_some() {
                        return Ok(lower);
                    } else {
                        println!("Unknown answer '{}'.\r", x);
                        continue 'outer;
                    }
                }
                Ctrl('c') => {
                    print!("\r\n");
                    process::exit(1);
                }
                Ctrl('d') => {
                    break;
                }
                Esc => {
                    print!("\r\n");
                    return Ok(abort);
                }
                _ => {
                    continue;
                }
            }
        }
        // We're only here if EOF was encountered
        return Err(format_err!("Encountered EOF while waiting for answer."));
    }
}

/// String-like things that have a *display* length.
///
/// Notably, this should not be the byte count (e.g ignore terminal formatting
/// characters).
pub trait Length {
    fn length(&self) -> usize;
}

impl<'a> Length for &'a str {
    fn length(&self) -> usize {
        self.len()
    }
}

impl Length for String {
    fn length(&self) -> usize {
        self.len()
    }
}

/// A string where different parts can have different colors.
///
/// This is a wrapper around [`ColoredString`][colored], which unfortunately can
/// only ever one color for the whole string.
///
/// [colored]: ../../colored/struct.ColoredString.html
pub struct MultiColorString {
    parts: Vec<ColoredString>,
}

impl MultiColorString {
    pub fn from_parts(parts: &[ColoredString]) -> Self {
        MultiColorString {
            parts: parts.to_owned(),
        }
    }
}

impl fmt::Display for MultiColorString {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut width = f.width();

        for part in &self.parts {
            write!(f, "{}", part)?;
            width = width.map(|x| x - part.len());
        }

        if width.map(|x| x > 0).unwrap_or(false) {
            write!(f, "{:width$}", "", width = width.unwrap())?;
        }

        Ok(())
    }
}

impl From<ColoredString> for MultiColorString {
    fn from(c: ColoredString) -> Self {
        MultiColorString { parts: vec![c] }
    }
}

impl Length for MultiColorString {
    fn length(&self) -> usize {
        self.parts.iter().map(|x| x.len()).sum()
    }
}

/// Colorize a diff between two strings.
///
/// This takes a diff as computed by [`lcs_diff`][lcs_diff] between two strings
/// and returns colorized versions of both strings.
///
/// Parts that are in the original and not in the new string are marked with
/// `diff_color_left` in the original, parts that are in the new but not the
/// original string are marked with `diff_color_right` in the new string.
///
/// Unchanged parts are either marked with [`Colorize::normal`][normal] or with
/// `normal_color_left` and `normal_color_right`.
///
/// [lcs_diff]: ../../lcs_diff/index.html
/// [normal]: ../../colored/trait.Colorize.html#tymethod.normal
pub fn render_diff(
    diff: &[DiffResult<&str>],
    diff_color_left: colored::Color,
    normal_color_left: Option<colored::Color>,
    diff_color_right: colored::Color,
    normal_color_right: Option<colored::Color>,
) -> (MultiColorString, MultiColorString) {
    use lcs_diff::DiffResult::*;

    let left_parts: Vec<ColoredString> = diff.iter()
        .filter_map(|res| match *res {
            Removed(ref elem) => Some(elem.data.color(diff_color_left)),
            Common(ref elem) => Some(maybe_colorize(elem.data, normal_color_left)),
            Added(_) => None,
        })
        .collect();

    let right_parts: Vec<ColoredString> = diff.iter()
        .filter_map(|res| match *res {
            Removed(_) => None,
            Common(ref elem) => Some(maybe_colorize(elem.data, normal_color_right)),
            Added(ref elem) => Some(elem.data.color(diff_color_right)),
        })
        .collect();

    (
        MultiColorString { parts: left_parts },
        MultiColorString { parts: right_parts },
    )
}

fn maybe_colorize<T>(source: T, color: Option<colored::Color>) -> ColoredString
where
    T: colored::Colorize,
{
    if let Some(c) = color {
        source.color(c)
    } else {
        source.normal()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn diff_normal() {
        use crate::srcinfo::Version;
        use colored::Color;

        let ver1 = Version::parse("1.2");
        let ver2 = Version::parse("1.3");
        let diff = ver1.diff(&ver2);

        let (left1, right1) = render_diff(&diff, Color::Red, None, Color::Green, None);
        let left2 = MultiColorString::from_parts(&["1.".normal(), "2".red()]);
        let right2 = MultiColorString::from_parts(&["1.".normal(), "3".green()]);

        assert_eq!(format!("{}", left1), format!("{}", left2));
        assert_eq!(format!("{}", right1), format!("{}", right2));
    }

    #[test]
    fn diff_colored() {
        use crate::srcinfo::Version;
        use colored::Color;

        let ver1 = Version::parse("1.2");
        let ver2 = Version::parse("1.3");
        let diff = ver1.diff(&ver2);

        let (left1, right1) = render_diff(
            &diff,
            Color::Red,
            Some(Color::Blue),
            Color::Green,
            Some(Color::Yellow),
        );
        let left2 = MultiColorString::from_parts(&["1".blue(), ".".blue(), "2".red()]);
        let right2 = MultiColorString::from_parts(&["1".yellow(), ".".yellow(), "3".green()]);

        assert_eq!(format!("{}", left1), format!("{}", left2));
        assert_eq!(format!("{}", right1), format!("{}", right2));
    }
}
